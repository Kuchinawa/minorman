#include <Windows.h>
#include <memory>
#include <time.h>

#include "MinorManEngine\GameObjects\GameObject.h"
#include "MinorManEngine\GameObjects\RenderGOComponent.h"
#include "MinorManEngine\GameObjects\TransformComponent.h"

#include "MinorManEngine\Graphics\Camera.h"
#include "MinorManEngine\Graphics\Dx11Context.h"
#include "MinorManEngine\Graphics\Sprite.h"
#include "MinorManEngine\Graphics\SpriteRenderer.h"
#include "MinorManEngine\Graphics\TileRenderer.h"

#include "MinorManEngine\Input\InputContext.h"

#include "MinorManEngine\Physics\Collision.h"

#include <FW1FontWrapper.h>

#include "Level.h"


using namespace MinorManEngine;

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
void Render();
void Update(const float dt);
void Shutdown();

//Timer Variables
LARGE_INTEGER timeCurrent, timeLast, timeElapsedMS;
LARGE_INTEGER timerFreq;

//DirectX Variables
std::shared_ptr<Dx11Context> directXContext(nullptr);
std::shared_ptr<SpriteRenderer> spriteRenderer(nullptr);

std::shared_ptr<Sprite> runningSprite(nullptr);
std::shared_ptr<Sprite> idleSprite(nullptr);
std::shared_ptr<Sprite> chargeSprite(nullptr);

//TileRenderer tileRenderer;

Sprite groundMSprite;

//Input Variables
InputContext inputContext;

//Text
IFW1Factory* fW1Factory;
IFW1FontWrapper* fontWrapper;

Level level;

int fps = 0;

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE prevInstance, LPWSTR cmdLine, int cmdShow)
{
    UNREFERENCED_PARAMETER(prevInstance);
    UNREFERENCED_PARAMETER(cmdLine);

    //Define the window properties
    WNDCLASSEX wndClass = { 0 };
    wndClass.cbSize = sizeof(WNDCLASSEX);
    wndClass.style = CS_HREDRAW | CS_VREDRAW;
    wndClass.lpfnWndProc = WndProc;
    wndClass.hInstance = hInstance;
    wndClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wndClass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wndClass.lpszMenuName = nullptr;
    wndClass.lpszClassName = L"MinorManClass";

    if (!RegisterClassEx(&wndClass))
        return -1;

    RECT rc = { 0, 0, 800, 600 };
    AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);

    HWND hwnd = CreateWindow(L"MinorManClass", L"MinorMan", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, nullptr, nullptr, hInstance, nullptr);

    if (!hwnd)
        return -1;

    ShowWindow(hwnd, cmdShow);

    bool result;

    //Initialize the input class
    result = inputContext.Initialize(hInstance, hwnd);

    if (!result)
    {
        DXTRACE_MSG(L"Failed to Initialize input!");
        return -1;
    }

    //Initialize the DX11 Device and Context
    directXContext = std::make_shared<Dx11Context>();
    result = directXContext->Initialize(hInstance, hwnd);

    if (!result)
    {
        DXTRACE_MSG(L"Failed to Initialize the DirectXContext!");
        return -1;
    }

    //Initialize the sprite renderer and load the shaders
    spriteRenderer = std::make_shared<SpriteRenderer>();
    spriteRenderer->LoadShaders(*directXContext.get());

    //Initialize the Font classes
    HRESULT hResult = FW1CreateFactory(FW1_VERSION, &fW1Factory);

    if (FAILED(hResult))
    {
        DXTRACE_MSG(L"Failed to load font");
        return -1;
    }


    hResult = fW1Factory->CreateFontWrapper(directXContext->GetD3dDevice(), L"Pirulen", &fontWrapper);

    if (FAILED(hResult))
    {
        DXTRACE_MSG(L"Failed to load font");
        return -1;
    }

    if (result == false)
        return -1;

    level = Level(hInstance, hwnd, directXContext, fontWrapper);

    QueryPerformanceCounter(&timeLast);

    double fpsTime = 0;
    int nbFrames = 0;

    MSG msg = { 0 };
    while (msg.message != WM_QUIT)
    {
        if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        else
        {
            //Get time
            QueryPerformanceFrequency(&timerFreq);
            QueryPerformanceCounter(&timeCurrent);

            timeElapsedMS.QuadPart = timeCurrent.QuadPart - timeLast.QuadPart; //Calculate elapsed time
            timeElapsedMS.QuadPart *= 1000; //Convert to Milliseconds
            double deltaMS = timeElapsedMS.QuadPart;
            deltaMS /= (double)timerFreq.QuadPart;

            fpsTime += deltaMS;
            nbFrames++;
            if (fpsTime >= 1000.0)
            {
                fps = (1000 / (1000 / (double)nbFrames));
                nbFrames = 0;
                fpsTime -= 1000.0;
            }

            //Update & Draw
            Update(deltaMS / 1000);
            //Update(0.016f);
            Render();

            timeLast = timeCurrent;
        }
    }

    Shutdown();

    return static_cast<int>(msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT paintStruct;
    HDC hDC;

    switch (message)
    {
    case WM_PAINT:
        hDC = BeginPaint(hwnd, &paintStruct);
        EndPaint(hwnd, &paintStruct);
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;

    default:
        return DefWindowProc(hwnd, message, wParam, lParam);
    }

    return 0;
}

void Update(const float dt)
{
    inputContext.Update();

    level.Update(dt, inputContext);
}

void Render()
{
    float clearColor[4] = { 0.0f, 0.2f, 0.5f, 0.0f };
    directXContext->ClearRenderTargetView(clearColor);

    level.Render();

    std::wstring drawString = L"FPS: " + std::to_wstring(fps);

    //Draw FPS
    //fontWrapper->DrawString(directXContext->GetD3dContext(), drawString.c_str(), 20.0f, 0.0f, 0.0f, 0xff00ffff, FW1_RESTORESTATE);

    directXContext->Swap();
}

void Shutdown()
{
    //testObject->Uninitialize();
    spriteRenderer->UnloadContent();
    directXContext->Shutdown();
    inputContext.Shutdown();
    fontWrapper->Release();
    fW1Factory->Release();
}