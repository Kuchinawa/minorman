#pragma once

#include "MinorManEngine\GameObjects\GameObject.h"

#include "MinorManEngine\Graphics\BackgroundLayer.h"
#include "MinorManEngine\Graphics\Texture.h"
#include "MinorManEngine\Graphics\TileRenderer.h"

#include "MinorManEngine\Input\InputContext.h"

#include "MinorManEngine\UI\UIElement.h"
#include "MinorManEngine\UI\UIHealth.h"

#include "MinorManEngine\Events\MeteorEventTrigger.h"

#include <string>

#include <FW1FontWrapper.h>

using namespace MinorManEngine;

class Level
{
public:
    Level();
    Level(HINSTANCE hInstance, HWND hwnd, const std::shared_ptr<Dx11Context>& directXContext, IFW1FontWrapper* fontWrapper);

    void Update(const float dt, InputContext &inputContext);
    void Render();

    void SpawnWalkCannon(float spawnX, float spawnY, std::vector<DirectX::XMFLOAT2> patrolPoints);

    void SpawnLaserCannon(float spawnX, float spawnY, bool orientation);

    void SpawnLava(float spawnX, float spawnY);

private:
    void HandleControls(const float dt, InputContext &inputContext);

    Camera camera_;

    std::shared_ptr<Dx11Context> directXContext_;
    std::shared_ptr<SpriteRenderer> spriteRenderer_;

    BackgroundLayer backgroundLayer_;
    TileRenderer tileRenderer_;

    std::shared_ptr<GameObject> player_;

    std::vector<std::shared_ptr<GameObject>> enemies_;

    MeteorEventTrigger meteorEventTrigger_;

    UIElement healthContainerUI_;
    UIHealth healthUI_;

    IFW1FontWrapper* fontWrapper_;

    std::unordered_map<std::string, std::shared_ptr<Texture>> textures_;
};