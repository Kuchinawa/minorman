#include "Level.h"

#include "MinorManEngine\Config.h"

#include "MinorManEngine\GameObjects\CollisionComponent.h"
#include "MinorManEngine\GameObjects\HeroControlComponent.h"
#include "MinorManEngine\GameObjects\HeroStatusComponent.h"
#include "MinorManEngine\GameObjects\RenderGOComponent.h"
#include "MinorManEngine\GameObjects\WalkCannonEnemyComponent.h"
#include "MinorManEngine\GameObjects\TransformComponent.h"
#include "MinorManEngine\GameObjects\StatusComponent.h"
#include "MinorManEngine\GameObjects\ProjectileEnemyComponent.h"
#include "MinorManEngine\GameObjects\LaserCannonComponent.h"
#include "MinorManEngine\GameObjects\LaserEnemyComponent.h"

#include "MinorManEngine\Graphics\SpriteFileDescriptions.h"

#include "MinorManEngine\Physics\Collision.h"

#include <algorithm>

using namespace DirectX;

Level::Level()
{

}

Level::Level(HINSTANCE hInstance, HWND hwnd, const std::shared_ptr<Dx11Context>& directXContext, IFW1FontWrapper* fontWrapper)
{
    directXContext_ = directXContext;

    fontWrapper_ = fontWrapper;

    //Create the spriteRenderer (Shader)
    spriteRenderer_ = std::make_shared<SpriteRenderer>();
    spriteRenderer_->LoadShaders(*directXContext_.get());


    //Load all the textures into the OUList textures_
    //Player/Hero
    textures_[SpriteFileDescriptions::HeroRunning.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::HeroRunning.fileName, directXContext_));
    textures_[SpriteFileDescriptions::HeroIdle.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::HeroIdle.fileName, directXContext_));
    textures_[SpriteFileDescriptions::HeroCharge.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::HeroCharge.fileName, directXContext_));
    textures_[SpriteFileDescriptions::HeroJump.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::HeroJump.fileName, directXContext_));
    textures_[SpriteFileDescriptions::HeroAttack.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::HeroAttack.fileName, directXContext_));

    //Enemies
    textures_[SpriteFileDescriptions::WalkCannonWalking.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::WalkCannonWalking.fileName, directXContext_));
    textures_[SpriteFileDescriptions::WalkCannonAttacking.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::WalkCannonAttacking.fileName, directXContext_));
    textures_[SpriteFileDescriptions::WalkCannonBullet.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::WalkCannonBullet.fileName, directXContext_));

    textures_[SpriteFileDescriptions::laserCannon.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::laserCannon.fileName, directXContext_));
    textures_[SpriteFileDescriptions::laserCannonShoot.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::laserCannonShoot.fileName, directXContext_));
    textures_[SpriteFileDescriptions::laser.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::laser.fileName, directXContext_));

    //Background
    textures_[SpriteFileDescriptions::Background.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::Background.fileName, directXContext_));

    //Terrain
    textures_[SpriteFileDescriptions::GroundM.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::GroundM.fileName, directXContext_));
    textures_[SpriteFileDescriptions::GroundM2.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::GroundM2.fileName, directXContext_));

    textures_[SpriteFileDescriptions::Lava.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::Lava.fileName, directXContext_));
    textures_[SpriteFileDescriptions::Meteor.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::Meteor.fileName, directXContext_));

    //UI
    textures_[SpriteFileDescriptions::HealthContainer.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::HealthContainer.fileName, directXContext_));
    textures_[SpriteFileDescriptions::HealthBar.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::HealthBar.fileName, directXContext_));
    textures_[SpriteFileDescriptions::HealthBarOne.fileName] = std::make_shared<Texture>(Texture(SpriteFileDescriptions::HealthBarOne.fileName, directXContext_));


    //Load the area textures and load the level

    std::vector<std::shared_ptr<Texture>> tileTextures;
    std::vector<SpriteFileDescription> spriteDescriptions;

    tileTextures.push_back(textures_[SpriteFileDescriptions::GroundM.fileName]);
    tileTextures.push_back(textures_[SpriteFileDescriptions::GroundM2.fileName]);

    spriteDescriptions.push_back(SpriteFileDescriptions::GroundM);
    spriteDescriptions.push_back(SpriteFileDescriptions::GroundM2);

    tileRenderer_.LoadSpriteCollection(directXContext_, spriteRenderer_, tileTextures, spriteDescriptions);
    tileRenderer_.LoadLevel(directXContext_, spriteRenderer_);

    //Load the background layer

    std::shared_ptr<Sprite> backgroundSprite(new Sprite(
        directXContext_,
        spriteRenderer_,
        textures_[SpriteFileDescriptions::Background.fileName],
        SpriteFileDescriptions::Background));

    backgroundLayer_ = BackgroundLayer(directXContext_, spriteRenderer_, backgroundSprite);

    //Create our hero
    player_ = std::make_shared<GameObject>();

    //Hero spawn location
    //std::unique_ptr<GOComponent> transformComp(new TransformComponent(1089, 600));
    std::unique_ptr<GOComponent> transformComp(new TransformComponent(321, 192));
    player_->AddComponent(std::move(transformComp));

    //Hero collision component
    std::unique_ptr<GOComponent> collisionComp(new CollisionComponent(player_, std::vector<XMFLOAT2>() = { { -17.0f, 12.0f }, { 17.0f, 12.0f }, { 17.0f, -32.0f }, { -17.0f, -32.0f } }, XMFLOAT2(17.0f, 22.0f), XMFLOAT2(17.0f, 22.0f)));
    player_->AddComponent(std::move(collisionComp));

    //Hero rendering
    std::shared_ptr<Sprite> idleSprite(new Sprite(directXContext_, spriteRenderer_, textures_[SpriteFileDescriptions::HeroIdle.fileName], SpriteFileDescriptions::HeroIdle, true));

    std::unique_ptr<GOComponent> renderComp(new RenderGOComponent(player_, directXContext_, spriteRenderer_, idleSprite));
    player_->AddComponent(std::move(renderComp));

    //Hero controls
    std::unique_ptr<HeroControlComponent> hControlComp(new HeroControlComponent(player_));
    player_->AddComponent(std::move(hControlComp));

    //Hero status
    std::unique_ptr<HeroStatusComponent> hStatusComp(new HeroStatusComponent(player_, Config::heroHealth, Config::heroDamage));
    player_->AddComponent(std::move(hStatusComp));

    player_->Initialize();


    ////Create enemies 

    //WalkCannons

    std::vector<XMFLOAT2> patrolPoints;
    patrolPoints.push_back(XMFLOAT2(576.0f, 208.0f));
    patrolPoints.push_back(XMFLOAT2(896.0f, 208.0f));
    SpawnWalkCannon(608, 208, patrolPoints);

    patrolPoints.clear();
    patrolPoints.push_back(XMFLOAT2(1088.0f, 208.0f));
    patrolPoints.push_back(XMFLOAT2(1408.0f, 208.0f));
    SpawnWalkCannon(1120, 208, patrolPoints);

    //laserCannons

    SpawnLaserCannon(1564, 224, true);
    SpawnLaserCannon(1828, 352, false);
    SpawnLaserCannon(1564, 480, true);
    SpawnLaserCannon(1828, 608, false);

    //Lava
    SpawnLava(1248, 459);
    SpawnLava(1312, 459);
    SpawnLava(1376, 459);
    SpawnLava(1440, 459);

    SpawnLava(928, 75);
    SpawnLava(992, 75);
    SpawnLava(1056, 75);


    //Meteor trigger
    std::vector<XMFLOAT2> meteorEventBB = { { -608.0f, 32.0f }, { 32.0f, 32.0f }, { 32.0f, -32.0f }, { -608.0f, -32.0f } };
    meteorEventTrigger_ = MeteorEventTrigger(meteorEventBB, XMFLOAT2(1184, 608), 3.0f);

    meteorEventTrigger_.AddSpawnPoint(XMFLOAT2(1064, 708), 0.25f);
    meteorEventTrigger_.AddSpawnPoint(XMFLOAT2(1000, 708), 0.5f);
    meteorEventTrigger_.AddSpawnPoint(XMFLOAT2(936, 708), 0.75f);
    meteorEventTrigger_.AddSpawnPoint(XMFLOAT2(872, 708), 1.0f);
    meteorEventTrigger_.AddSpawnPoint(XMFLOAT2(808, 708), 1.25f);
    meteorEventTrigger_.AddSpawnPoint(XMFLOAT2(744, 708), 1.5f);
    meteorEventTrigger_.AddSpawnPoint(XMFLOAT2(680, 708), 1.75f);
    meteorEventTrigger_.AddSpawnPoint(XMFLOAT2(616, 708), 2.0f);
    meteorEventTrigger_.AddSpawnPoint(XMFLOAT2(552, 708), 2.25f);


    ////UI
    //HP Container
    std::shared_ptr<Sprite> healthContainerSprite(new Sprite(
        directXContext_,
        spriteRenderer_,
        textures_[SpriteFileDescriptions::HealthContainer.fileName],
        SpriteFileDescriptions::HealthContainer));

    healthContainerUI_ = UIElement(
        healthContainerSprite,
        directXContext_,
        spriteRenderer_,
        XMFLOAT2(Config::healthBarPositionX, Config::healthBarPositionY));

    //HP Bar
    std::shared_ptr<Sprite> healthSprite(new Sprite(
        directXContext_,
        spriteRenderer_,
        textures_[SpriteFileDescriptions::HealthBar.fileName],
        SpriteFileDescriptions::HealthBar));

    healthUI_ = UIHealth(
        healthSprite,
        directXContext_,
        spriteRenderer_,
        XMFLOAT2(Config::healthBarPositionX, Config::healthBarPositionY + 14.0f));
}

//Helper function that checks if a gameobject is still alive
//If dead uninitialize
bool IsDeadAndUninit(std::shared_ptr<GameObject> Character)
{
    if (Character->GetComponent<StatusComponent>() != nullptr)
    {
        if (!Character->GetComponent<StatusComponent>()->IsAlive())
        {
            Character->Uninitialize();
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

void Level::Update(const float dt, InputContext &inputContext)
{
    HandleControls(dt, inputContext);

    player_->Update(dt);

    //Reposition the camera
    camera_.Update(dt, player_->GetComponent<TransformComponent>()->GetPosition(), tileRenderer_.GetAABB());

    //Update enemies
    for (int i = 0; i < enemies_.size(); i++)
    {
        if (enemies_.at(i)->GetComponent<EnemyComponent>() != nullptr)
        {
            enemies_.at(i)->GetComponent<EnemyComponent>()->UpdateAI(dt, player_.get());
        }
        else if (enemies_.at(i)->GetComponent<WalkCannonEnemyComponent>() != nullptr)
        {
            enemies_.at(i)->GetComponent<WalkCannonEnemyComponent>()->UpdateAI(dt, player_.get(), directXContext_, spriteRenderer_, textures_, enemies_);
        }
        else if (enemies_.at(i)->GetComponent<ProjectileEnemyComponent>() != nullptr)
        {
            enemies_.at(i)->GetComponent<ProjectileEnemyComponent>()->UpdateAI(dt, player_.get(), tileRenderer_);
        }
        else if (enemies_.at(i)->GetComponent<LaserCannonComponent>() != nullptr)
        {
            enemies_.at(i)->GetComponent<LaserCannonComponent>()->UpdateAI(dt, player_.get(), directXContext_, spriteRenderer_, textures_, enemies_);
        }
        else if (enemies_.at(i)->GetComponent<LaserEnemyComponent>() != nullptr)
        {
            enemies_.at(i)->GetComponent<LaserEnemyComponent>()->UpdateAI(dt, player_.get());
        }

        enemies_.at(i)->Update(dt);
    }

    //Remove dead enemies
    enemies_.erase(std::remove_if(enemies_.begin(), enemies_.end(), IsDeadAndUninit), enemies_.end());

    //Update triggers
    meteorEventTrigger_.Update(dt, player_.get(), directXContext_, spriteRenderer_, textures_, enemies_);

    //Update UI
    healthUI_.UpdateHealth(player_->GetComponent<HeroStatusComponent>()->GetHealth(), Config::heroHealth);
}

void Level::HandleControls(const float dt, InputContext &inputContext)
{
    player_->GetComponent<HeroControlComponent>()->HandleInput(dt, player_.get(), directXContext_, spriteRenderer_, textures_, inputContext, tileRenderer_, enemies_);
}

void Level::Render()
{
    backgroundLayer_.Render(camera_);
    tileRenderer_.Render(camera_);

    for (int i = 0; i < enemies_.size(); i++)
    {
        enemies_.at(i)->GetComponent<RenderGOComponent>()->Render(camera_);
    }

    player_->GetComponent<RenderGOComponent>()->Render(camera_);

    healthContainerUI_.Render();
    healthUI_.Render();

    fontWrapper_->DrawString(directXContext_->GetD3dContext(),
        std::to_wstring(player_->GetComponent<HeroStatusComponent>()->GetHealth()).c_str(), //Text to draw
        16.0f, //Font size
        31.0f, //location X
        250.0f, //Location Y
        0xff000000, //Color
        FW1_RESTORESTATE); //Restore the original draw settings

    //XMVECTOR position = player_->GetComponent<TransformComponent>()->GetPosition();

    //std::wstring posStr = L"X: " + std::to_wstring((int)XMVectorGetX(position)) + L" Y: " + std::to_wstring((int)XMVectorGetY(position));

    //fontWrapper_->DrawString(directXContext_->GetD3dContext(),
    //    posStr.c_str(),
    //    20.0f, //Font size
    //    0.0f, //location X
    //    40.0f, //Location Y
    //    0xff00ffff, //Color
    //    FW1_RESTORESTATE); //Restore the original draw settings

}

void Level::SpawnWalkCannon(float spawnX, float spawnY, std::vector<XMFLOAT2> patrolPoints)
{
    //Create enemy (WalkCannon)
    std::shared_ptr<GameObject> walkCannon = std::make_shared<GameObject>();

    //Spawn location
    std::unique_ptr<GOComponent> wcTransformComp(new TransformComponent(spawnX, spawnY));
    walkCannon->AddComponent(std::move(wcTransformComp));

    //Collision component
    std::unique_ptr<GOComponent> wcCollisionComp(new CollisionComponent(walkCannon, std::vector<XMFLOAT2>() = { { -16.0f, 16.0f }, { 16.0f, 16.0f }, { 16.0f, -16.0f }, { -16.0f, -16.0f } }, XMFLOAT2(32.0f, 32.0f), XMFLOAT2(16.0f, 16.0f)));
    walkCannon->AddComponent(std::move(wcCollisionComp));

    //Rendering
    std::shared_ptr<Sprite> wcWalkSprite(new Sprite(
        directXContext_,
        spriteRenderer_,
        textures_[SpriteFileDescriptions::WalkCannonWalking.fileName],
        SpriteFileDescriptions::WalkCannonWalking,
        false));
    std::unique_ptr<GOComponent> wcRenderComp(new RenderGOComponent(walkCannon, directXContext_, spriteRenderer_, wcWalkSprite));
    walkCannon->AddComponent(std::move(wcRenderComp));

    //Status
    std::unique_ptr<StatusComponent> statusComp(new StatusComponent(Config::walkCannonHealth, Config::walkCannonDamage));
    walkCannon->AddComponent(std::move(statusComp));

    //AI
    std::unique_ptr<WalkCannonEnemyComponent> wcEnemyComp(new WalkCannonEnemyComponent(walkCannon, XMFLOAT2(Config::walkCannonSpeed, 0.0f), true));
    walkCannon->AddComponent(std::move(wcEnemyComp));

    for (int i = 0; i < patrolPoints.size(); i++)
    {
        walkCannon->GetComponent<WalkCannonEnemyComponent>()->AddPatrolPoint(patrolPoints.at(i));
    }

    walkCannon->Initialize();

    enemies_.push_back(walkCannon);
}

void Level::SpawnLaserCannon(float spawnX, float spawnY, bool orientation)
{
    //Create enemy (WalkCannon)
    std::shared_ptr<GameObject> laserCannon = std::make_shared<GameObject>();

    //Spawn location
    std::unique_ptr<GOComponent> lcTransformComp(new TransformComponent(spawnX, spawnY));
    laserCannon->AddComponent(std::move(lcTransformComp));

    //Collision component
    std::unique_ptr<GOComponent> lcCollisionComp(new CollisionComponent(laserCannon, std::vector<XMFLOAT2>() = { { -14.0f, 20.0f }, { 14.0f, 20.0f }, { 14.0f, -20.0f }, { -14.0f, -20.0f } }, XMFLOAT2(64.0f, 32.0f), XMFLOAT2(14.0f, 20.0f)));
    laserCannon->AddComponent(std::move(lcCollisionComp));

    //Rendering
    std::shared_ptr<Sprite> lcLaserCannon(new Sprite(
        directXContext_,
        spriteRenderer_,
        textures_[SpriteFileDescriptions::laserCannon.fileName],
        SpriteFileDescriptions::laserCannon,
        orientation,
        false));
    std::unique_ptr<GOComponent> lcRenderComp(new RenderGOComponent(laserCannon, directXContext_, spriteRenderer_, lcLaserCannon));
    laserCannon->AddComponent(std::move(lcRenderComp));

    //Status
    std::unique_ptr<StatusComponent> statusComp(new StatusComponent(Config::laserCannonHealth, Config::laserCannonDamage));
    laserCannon->AddComponent(std::move(statusComp));

    //AI
    std::unique_ptr<LaserCannonComponent> lcEnemyComp(new LaserCannonComponent(laserCannon, true, orientation));
    laserCannon->AddComponent(std::move(lcEnemyComp));

    laserCannon->Initialize();

    enemies_.push_back(laserCannon);
}

void Level::SpawnLava(float spawnX, float spawnY)
{
    //Create lava
    std::shared_ptr<GameObject> lava = std::make_shared<GameObject>();

    //Spawn
    std::unique_ptr<GOComponent> lavaTransformComp(new TransformComponent(spawnX, spawnY));
    lava->AddComponent(std::move(lavaTransformComp));

    //Add collision data
    std::unique_ptr<GOComponent> collisionComp(new CollisionComponent(lava, std::vector<XMFLOAT2>() = { { -32.0f, 11.0f }, { 32.0f, 11.0f }, { 32.0f, -11.0f }, { -32.0f, -11.0f } }, XMFLOAT2(32.0f, 11.0f), XMFLOAT2(32.0f, 11.0f)));
    lava->AddComponent(std::move(collisionComp));

    //Rendering
    std::shared_ptr<Sprite> lavaSprite(new Sprite(
        directXContext_,
        spriteRenderer_,
        textures_[SpriteFileDescriptions::Lava.fileName],
        SpriteFileDescriptions::Lava));
    std::unique_ptr<GOComponent> lavaRenderComp(new RenderGOComponent(lava, directXContext_, spriteRenderer_, lavaSprite));
    lava->AddComponent(std::move(lavaRenderComp));

    //Status
    std::unique_ptr<StatusComponent> lavaStatusComp(new StatusComponent(Config::lavaHealth, Config::lavaDamage, true));
    lava->AddComponent(std::move(lavaStatusComp));

    //AI
    std::unique_ptr<EnemyComponent> lavaEnemyComp(new EnemyComponent(lava, XMFLOAT2(0.0f, 0.0f), true));
    lava->AddComponent(std::move(lavaEnemyComp));

    lava->Initialize();

    enemies_.push_back(lava);
}

