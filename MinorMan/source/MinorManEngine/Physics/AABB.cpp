#include "AABB.h"

using namespace MinorManEngine;
using namespace DirectX;

AABB::AABB()
{
    center_ = XMFLOAT2(0, 0);
    halfExtends_ = XMFLOAT2(0, 0);
}

AABB::AABB(XMFLOAT2 center, XMFLOAT2 halfExtends)
{
    center_ = center;
    halfExtends_ = halfExtends;
}

XMVECTOR AABB::GetCenter() const
{
    return XMLoadFloat2(&center_);
}

XMVECTOR AABB::GetHalfExtends() const
{
    return XMLoadFloat2(&halfExtends_);
}

void AABB::SetCenter(float x, float y)
{
    center_.x = x;
    center_.y = y;
}

void AABB::SetCenter(XMVECTOR newCenter)
{
    XMStoreFloat2(&center_, newCenter);
}

void AABB::SetHalfExtends(float x, float y)
{
    halfExtends_.x = x;
    halfExtends_.y = y;
}

XMVECTOR AABB::GetTopLeft() const
{
    return XMVectorAdd(XMLoadFloat2(&center_), XMLoadFloat2(new XMFLOAT2(-halfExtends_.x, halfExtends_.y)));
}

XMVECTOR AABB::GetTopRight() const
{
    return XMVectorAdd(XMLoadFloat2(&center_), XMLoadFloat2(&halfExtends_));
}

XMVECTOR AABB::GetBottomLeft() const
{
    return XMVectorAdd(XMLoadFloat2(&center_), XMLoadFloat2(new XMFLOAT2(-halfExtends_.x, -halfExtends_.y)));
}

XMVECTOR AABB::GetBottomRight() const
{
    return XMVectorAdd(XMLoadFloat2(&center_), XMLoadFloat2(new XMFLOAT2(halfExtends_.x, -halfExtends_.y)));
}