#include "Collision.h"

using namespace MinorManEngine;
using namespace DirectX;

//Check if two shapes collided using GJK
bool Collision::GJK2DCollision(const std::vector<XMVECTOR> &shapeA, const std::vector<XMVECTOR> &shapeB)
{
    XMVECTOR direction = XMVectorSet(-1, 0, 0, 0);

    //Find furthest point in the direction
    std::vector<XMVECTOR> lastPoints;
    lastPoints.push_back(Support(shapeA, shapeB, direction));
    direction = XMVectorNegate(lastPoints.back());

    while (true)
    {
        //Find point in the new direction
        XMVECTOR A = Support(shapeA, shapeB, direction);

        //Did we go past 0?
        if (XMVectorGetX(XMVector2Dot(A, direction)) < 0)
        {//If not, No intersection
            return false;
        }

        lastPoints.push_back(A);
        if (DoSimplex(lastPoints, direction))
        {//If true collision
            return true;
        }
    }
}

//Check if two shapes collided using GJK, if they do, return the depth and depth vector using EPA
bool Collision::GJK2DCollision(const std::vector<XMVECTOR> &shapeA, const std::vector<XMVECTOR> &shapeB, double &depth, XMVECTOR &depthVector)
{
    XMVECTOR direction = XMVectorSet(-1, 0, 0, 0);

    //Find furthest point in the direction
    std::vector<XMVECTOR> lastPoints;//Inverted
    lastPoints.push_back(Support(shapeA, shapeB, direction));
    direction = XMVectorNegate(lastPoints.back());

    while (true)
    {
        //Find point in the new direction
        XMVECTOR A = Support(shapeA, shapeB, direction);

        //Did we go past 0?
        if (XMVectorGetX(XMVector2Dot(A, direction)) < 0)
        {//If not, No intersection
            return false;
        }

        lastPoints.push_back(A);
        if (DoSimplex(lastPoints, direction))
        {//If true collision

            //Get the winding order (reverse because lastPoints is inverted)
            bool WINDINGCLOCKWISE = !GetWindingDirection(lastPoints);
            //EPA
            while (true)
            {
                double tolerance = 0.00001;

                //Get the edge closest to the origin
                Edge e = FindClosestEdge(lastPoints, WINDINGCLOCKWISE);
                //Obtain a new support point in the direction of the edge normal
                XMVECTOR p = Support(shapeA, shapeB, e.normal);

                //Check the distance from the origin to the edge
                double d = XMVectorGetX(XMVector2Dot(p, e.normal));
                if (d - e.distance < tolerance)
                {
                    //If less then the tolerance we assume we cant expand the simplex further
                    //And we return our results
                    depthVector = e.normal;
                    depth = d;
                    return true;
                }
                else
                {
                    //We haven't reached the edge of the minkowski sum
                    //Continue expanding adding the new point to the simplex
                    //in between the points that made the closest edge
                    lastPoints.insert(lastPoints.begin() + e.index, p);
                }

            }
        }
    }
}

Collision::Edge Collision::FindClosestEdge(const std::vector<XMVECTOR> &simplex, const bool &windingClockwise)
{
    Edge closest;
    closest.distance = std::numeric_limits<double>::infinity();

    for (int i = 0; i < simplex.size(); i++)
    {
        //Get the next index
        int j = i + 1 == simplex.size() ? 0 : i + 1;

        //Get the current and next vector
        XMVECTOR a = simplex.at(i);
        XMVECTOR b = simplex.at(j);

        //Create the edge vector
        XMVECTOR e = XMVectorSubtract(b, a);

        //Get the vector from origin to a
        XMVECTOR oa = a;

        //Get the vector from the edge towards the origin
        XMVECTOR n = e;
        if (windingClockwise == true)
        {

            float nx = XMVectorGetX(n);
            float ny = XMVectorGetY(n);
            n = XMVectorSetX(n, ny);
            n = XMVectorSetY(n, -nx);
        }
        else
        {
            float nx = XMVectorGetX(n);
            float ny = XMVectorGetY(n);
            n = XMVectorSetX(n, -ny);
            n = XMVectorSetY(n, nx);
        }

        n = XMVector2Normalize(n);

        //Calculate the distance from the origin to the edge
        double d = XMVectorGetX(XMVector2Dot(n, a));

        if (d < closest.distance)
        {
            closest.distance = d;
            closest.normal = n;
            closest.index = j;
        }
    }

    return closest;

}

bool Collision::GetWindingDirection(const std::vector<DirectX::XMVECTOR> &simplex)
{
    int n = simplex.size();
    double area = 0;


    for (int p = n - 1, q = 0; q < n; p = q, q++)
    {
        area += ((double)XMVectorGetX(simplex.at(p)) * ((double)XMVectorGetY(simplex.at(q))) - ((double)XMVectorGetX(simplex.at(q))) * ((double)XMVectorGetY(simplex.at(p))));
    }

    if (area < 0)
    {
        //Clockwise
        return true;
    }
    else
    {
        //Counter clockwise
        return false;
    }
}

//Check the distance between two (non-colliding) objects using GJK
bool Collision::GJK2DDistance(const std::vector<XMVECTOR> &shapeA, const std::vector<XMVECTOR> &shapeB, double &distance)
{
    double tolerance = 0.01;
    XMVECTOR direction = XMVectorSet(6, -4.5, 0, 0);

    //Find furthest point in the direction
    XMVECTOR A = Support(shapeA, shapeB, direction);
    direction = XMVectorNegate(A);
    XMVECTOR B = Support(shapeA, shapeB, direction);


    while (true)
    {
        //Optain the point on the current simplex closest to the origin
        XMVECTOR p = ClosestPointToOrigin(A, B);

        //Check if P is on the origin
        if (XMVector2Equal(p, XMVectorZero()))
        {
            //Collision
            return true;
        }

        //p is the new direction
        direction = XMVector2Normalize(XMVectorNegate(p));
        //Get a new Minkowski point along the direction
        XMVECTOR C = Support(shapeA, shapeB, direction);

        //Is the point we obtained making progess towards the origin?
        double dc = XMVectorGetX(XMVector2Dot(C, direction));
        double da = XMVectorGetX(XMVector2Dot(A, direction));

        //If we havent made enough progress (given some tolerance) we can assume we are done
        if (dc - da < tolerance)
        {
            distance = dc;
            return false;
        }

        //Only keep values closest to the origin
        if (Magnitude(A) < Magnitude(B))
        {
            B = C;
        }
        else
        {
            A = C;
        }

    }
}

//Check the distance between two (non-colliding) objects using GJK also gives the closest points
bool Collision::GJK2DDistance(const std::vector<XMVECTOR> &shapeA, const std::vector<XMVECTOR> &shapeB, double &distance, XMVECTOR &ClosestA, XMVECTOR &ClosestB)
{
    double tolerance = 0.01;
    XMVECTOR direction = XMVectorSet(6, -4.5, 0, 0);

    //Find furthest point in the direction

    std::vector<XMVECTOR> shapePointsA;
    XMVECTOR A = Support(shapeA, shapeB, direction, shapePointsA);

    direction = XMVectorNegate(A);
    std::vector<XMVECTOR> shapePointsB;
    XMVECTOR B = Support(shapeA, shapeB, direction, shapePointsB);


    while (true)
    {
        //Optain the point on the current simplex closest to the origin
        XMVECTOR p = ClosestPointToOrigin(A, B);

        //Check if P is on the origin
        if (XMVector2Equal(p, XMVectorZero()))
        {
            //Collision
            return true;
        }

        //p is the new direction
        direction = XMVector2Normalize(XMVectorNegate(p));
        //Get a new Minkowski point along the direction
        std::vector<XMVECTOR> shapePointsC;
        XMVECTOR C = Support(shapeA, shapeB, direction, shapePointsC);

        //Is the point we obtained making progess towards the origin?
        double dc = XMVectorGetX(XMVector2Dot(C, direction));
        double da = XMVectorGetX(XMVector2Dot(A, direction));

        //If we havent made enough progress (given some tolerance) we can assume we are done
        if (dc - da < tolerance)
        {
            distance = dc;

            //Get the closest points by finding the point on the termination simplex 
            //that forms a perpendicular line with the origin

            XMVECTOR L = B - A;

            //Check if the points are the same (preventing a divide by zero)
            //If they are return the support points
            if (XMVector2Equal(L, XMVectorZero()))
            {
                ClosestA = shapePointsA.at(0);
                ClosestB = shapePointsA.at(1);
            }
            else
            {
                double LdotL = XMVectorGetX(XMVector2Dot(L, L));
                double negLdotA = XMVectorGetX(XMVector2Dot(XMVectorNegate(L), A));

                double lambda2 = negLdotA / LdotL;
                double lambda1 = 1 - lambda2;

                //If one of the lambda's is negative the support points
                //of the other minkowski sum are the closest points
                if (lambda1 < 0)
                {
                    ClosestA = shapePointsB.at(0);
                    ClosestB = shapePointsB.at(1);
                }
                else if (lambda2 < 0)
                {
                    ClosestA = shapePointsA.at(0);
                    ClosestB = shapePointsA.at(1);
                }
                else
                {
                    //Else solve the closest points
                    ClosestA = (lambda1 * shapePointsA.at(0)) + (lambda2 * shapePointsB.at(0));
                    ClosestB = (lambda1 * shapePointsA.at(1)) + (lambda2 * shapePointsB.at(1));;
                }
            }

            return false;
        }

        //Only keep values closest to the origin
        if (Magnitude(A) < Magnitude(B))
        {
            shapePointsB = shapePointsC;
            B = C;
        }
        else
        {
            shapePointsA = shapePointsC;
            A = C;
        }

    }
}

bool Collision::DoSimplex(std::vector<XMVECTOR> &shape, XMVECTOR &direction)
{
    //Get the AO, same as -A
    XMVECTOR lineAO = XMVectorNegate(shape.back());

    if (shape.size() == 2)
    {
        XMVECTOR lineAB = XMVectorSubtract(shape.front(), shape.back());

        //If same direction (between A & B)
        if (XMVectorGetX(XMVector2Dot(lineAB, lineAO)) > 0)
        {
            direction = TripleProduct(lineAB, lineAO, lineAB);
            return false;
        }
        else
        {
            shape = { shape.back() };
            direction = XMVector2Normalize(lineAO);
            return false;
        }
    }
    else if (shape.size() == 3)
    {
        //Compute the edges
        XMVECTOR lineAB = shape.at(1) - shape.at(2);
        XMVECTOR lineAC = shape.at(0) - shape.at(2);

        XMVECTOR ABC = XMVectorMultiply(lineAB, lineAC);

        //Get the perpendicular lines of AB and AC
        XMVECTOR perpAB = TripleProduct(lineAC, lineAB, lineAB);
        XMVECTOR perpAC = TripleProduct(lineAB, lineAC, lineAC);

        //Origin in AB Region?
        if (XMVectorGetX(XMVector2Dot(perpAB, lineAO)) > 0)
        {
            //Remove C
            shape.erase(shape.begin());
            direction = perpAB;
            return false;
        }
        else
        {   //Origin in AC Region?
            if (XMVectorGetX(XMVector2Dot(perpAC, lineAO)) > 0)
            {
                //Remove B
                shape.erase(shape.begin() + 1);
                direction = perpAC;
                return false;
            }
            else
            {
                //Collision
                return true;
            }
        }
    }
    return false;
}

XMVECTOR Collision::TripleProduct(const XMVECTOR &A, const XMVECTOR &B, const XMVECTOR &C)
{
    double AC = XMVectorGetX(XMVector2Dot(A, C));
    double BC = XMVectorGetX(XMVector2Dot(B, C));

    XMVECTOR result = (B * AC) - (A * BC);

    return result;
}

XMVECTOR Collision::Support(const std::vector<XMVECTOR> &ShapeA, const std::vector<XMVECTOR> &ShapeB, const XMVECTOR direction)
{
    //Get furthest points
    XMVECTOR p1 = GetFurthestPoint(ShapeA, direction);
    XMVECTOR p2 = GetFurthestPoint(ShapeB, XMVectorNegate(direction));

    return XMVectorSubtract(p1, p2);
}

XMVECTOR Collision::Support(const std::vector<XMVECTOR> &ShapeA, const std::vector<XMVECTOR> &ShapeB, const XMVECTOR direction, std::vector<XMVECTOR> &shapePointsUsed)
{
    //Get furthest points
    XMVECTOR p1 = GetFurthestPoint(ShapeA, direction);
    XMVECTOR p2 = GetFurthestPoint(ShapeB, XMVectorNegate(direction));

    //Store points used
    shapePointsUsed.insert(shapePointsUsed.begin(), p1);
    shapePointsUsed.insert(shapePointsUsed.begin() + 1, p2);

    return XMVectorSubtract(p1, p2);
}

XMVECTOR Collision::GetFurthestPoint(const std::vector<XMVECTOR> shape, const XMVECTOR direction)
{
    int index = 0;
    XMVECTOR maxValue = XMVector2Dot(shape.at(0), direction);

    for (unsigned int i = 0; i < shape.size(); i++)
    {
        XMVECTOR value = XMVector2Dot(shape.at(i), direction);
        if (XMVectorGetX(value) > XMVectorGetX(maxValue))
        {
            maxValue = value;
            index = i;
        }
    }

    return shape.at(index);
}

XMVECTOR Collision::ClosestPointToOrigin(const XMVECTOR &A, const XMVECTOR &B)
{
    //Get the lines
    XMVECTOR AB = B - A;
    XMVECTOR AO = XMVectorNegate(A);

    //Project AO onto AB
    double AODotAB = XMVectorGetX(XMVector2Dot(AO, AB));
    //Get the length squared
    double ABSquared = XMVectorGetX(XMVector2Dot(AB, AB));

    //Calculate the distance along AB
    double t = AODotAB / ABSquared;

    //Calculate the point
    XMVECTOR closestPoint = AB * t + A;

    return closestPoint;
}

double Collision::Magnitude(const XMVECTOR &A)
{
    XMVECTOR magVec = XMVectorPow(A, XMLoadFloat2(new XMFLOAT2(2, 2)));
    double magnitude = sqrt(XMVectorGetX(magVec) + XMVectorGetY(magVec));

    return magnitude;
}

bool Collision::AABBvsAABB(const AABB &shapeA, const AABB &shapeB, float &distance, XMVECTOR &normal, XMVECTOR &center)
{
    center = shapeA.GetCenter();
    XMVECTOR combinedExtends = shapeB.GetHalfExtends() + shapeA.GetHalfExtends();
    XMVECTOR combinedPos = shapeB.GetCenter();

    XMVECTOR delta = combinedPos - shapeA.GetCenter();

    //Get distance & direction

    //Form closest plane to point
    XMVECTOR planeNormal; //Normal

    //Get the major Axis
    if (fabsf(XMVectorGetX(delta)) > fabsf(XMVectorGetY(delta)))
    {
        planeNormal = XMVector2Normalize(XMVectorNegate(XMVectorSetY(delta, 0.0f)));
        
    }
    else
    {
        planeNormal = XMVector2Normalize(XMVectorNegate(XMVectorSetX(delta, 0.0f)));
    }

    normal = planeNormal;

    XMVECTOR planeCenter = XMVectorMultiplyAdd(planeNormal, combinedExtends, combinedPos);
        
    //Distance point from plane
    XMVECTOR planeDelta = shapeA.GetCenter() - planeCenter;
    distance = XMVectorGetX(XMVector2Dot(planeDelta, planeNormal));
    

    return true;
}

bool Collision::MovingTowardsX(const DirectX::XMVECTOR pointA, const DirectX::XMVECTOR velocityA, const DirectX::XMVECTOR pointB)
{
    float delta = XMVectorGetX(pointB) - XMVectorGetX(pointA);
    
    return delta * XMVectorGetX(velocityA) > 0;
}