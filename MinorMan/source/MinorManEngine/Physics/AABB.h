#pragma once

#include <DirectXMath.h>

namespace MinorManEngine
{
    //An axis-aligned bounding box
    class AABB
    {
    public:

        AABB();
        AABB(DirectX::XMFLOAT2 center, DirectX::XMFLOAT2 halfExtends);

        DirectX::XMVECTOR GetCenter() const;
        DirectX::XMVECTOR GetHalfExtends() const;

        void SetCenter(float x, float y);
        void SetCenter(DirectX::XMVECTOR newCenter);
        void SetHalfExtends(float x, float y);

        DirectX::XMVECTOR GetTopLeft() const;
        DirectX::XMVECTOR GetTopRight() const;
        DirectX::XMVECTOR GetBottomLeft() const;
        DirectX::XMVECTOR GetBottomRight() const;

    private:
        DirectX::XMFLOAT2 center_;
        DirectX::XMFLOAT2 halfExtends_;

        
    };
}