//Written by Gert "Kuchinawa" Meijer
//Theory from http://www.codezealot.org
#pragma once

#include <vector>
#include <DirectXMath.h>
#include "AABB.h"

namespace MinorManEngine
{
    class Collision
    {
    public:

        //Check if two shapes collided using GJK
        static bool GJK2DCollision(const std::vector<DirectX::XMVECTOR> &shapeA, const std::vector<DirectX::XMVECTOR> &shapeB);

        //Check if two shapes collided using GJK, if they do, return the depth and depth vector using EPA
        static bool GJK2DCollision(const std::vector<DirectX::XMVECTOR> &shapeA, const std::vector<DirectX::XMVECTOR> &shapeB, double &depth, DirectX::XMVECTOR &depthVector);

        //Check the distance between two (non-colliding) objects using GJK
        static bool GJK2DDistance(const std::vector<DirectX::XMVECTOR> &shapeA, const std::vector<DirectX::XMVECTOR> &shapeB, double &distance);

        //Check the distance between two (non-colliding) objects using GJK also gives the closest points
        static bool GJK2DDistance(const std::vector<DirectX::XMVECTOR> &shapeA, const std::vector<DirectX::XMVECTOR> &shapeB, double &distance, DirectX::XMVECTOR &ClosestA, DirectX::XMVECTOR &ClosestB);

        //Determine the distance between two AABBs
        static bool AABBvsAABB(const AABB &shapeA, const AABB &shapeB, float &distance, DirectX::XMVECTOR &normal, DirectX::XMVECTOR &center);

        //Determine if a point A is moving towards point B (only X axis)
        static bool MovingTowardsX(const DirectX::XMVECTOR pointA, const DirectX::XMVECTOR velocityA, const DirectX::XMVECTOR pointB);

    private:
        Collision();

        struct Edge
        {
            double distance;
            DirectX::XMVECTOR normal;
            int index;
        };

        static bool DoSimplex(std::vector<DirectX::XMVECTOR> &shape, DirectX::XMVECTOR &direction);
        
        //Get furthest point opposite points on minkowski sum of the gives shapes in the given direction
        static DirectX::XMVECTOR Support(const std::vector<DirectX::XMVECTOR> &shapeA, const std::vector<DirectX::XMVECTOR> &shapeB, const DirectX::XMVECTOR direction);
        //Get furthest point opposite points on minkowski sum of the gives shapes in the given direction and store the shape points used
        static DirectX::XMVECTOR Support(const std::vector<DirectX::XMVECTOR> &ShapeA, const std::vector<DirectX::XMVECTOR> &ShapeB, const DirectX::XMVECTOR direction, std::vector<DirectX::XMVECTOR> &shapePointsUsed);
        
        static DirectX::XMVECTOR GetFurthestPoint(const std::vector<DirectX::XMVECTOR> shape, const DirectX::XMVECTOR direction);
        
        static DirectX::XMVECTOR TripleProduct(const DirectX::XMVECTOR &A, const DirectX::XMVECTOR &B, const DirectX::XMVECTOR &C);
        
        static DirectX::XMVECTOR ClosestPointToOrigin(const DirectX::XMVECTOR &A, const DirectX::XMVECTOR &B);
      
        static double Magnitude(const DirectX::XMVECTOR &A);

        //Returns True for Clockwise and False for Counter-Clockwise
        static bool GetWindingDirection(const std::vector<DirectX::XMVECTOR> &simplex);

        //Gets the edge of the given simplex thats closest to the origin
        static Edge FindClosestEdge(const std::vector<DirectX::XMVECTOR> &simplex, const bool &windingClockwise);

        
    };
}