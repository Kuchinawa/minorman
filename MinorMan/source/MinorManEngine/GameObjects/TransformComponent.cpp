#include "TransformComponent.h"

using namespace MinorManEngine;

using namespace DirectX;

TransformComponent::TransformComponent(const float x, const float y) : GOComponent(), rotation_(0)
{
    position_.x = x;
    position_.y = y;
    scale_.x = scale_.y = 1.0f;

    velocityX_ = 0.0f;
    velocityY_ = 0.0f;
}


TransformComponent::~TransformComponent()
{

}

bool TransformComponent::Initialize()
{
    return true;
}

XMMATRIX TransformComponent::GetWorldMatrix()
{
    XMMATRIX translation = XMMatrixTranslation(position_.x, position_.y, 0.0f);
    XMMATRIX rotationZ = XMMatrixRotationZ(rotation_);
    XMMATRIX scale = XMMatrixScaling(scale_.x, scale_.y, 1.0f);

    return translation * rotationZ * scale;
}

XMVECTOR TransformComponent::GetPosition()
{
    return XMLoadFloat2(&position_);
}

XMVECTOR TransformComponent::GetVelocity()
{
    return XMLoadFloat2(new XMFLOAT2(velocityX_, velocityY_));
}

float TransformComponent::GetVelocityX()
{
    return velocityX_;
}

float TransformComponent::GetVelocityY()
{
    return velocityY_;
}

void TransformComponent::SetPosition(const XMFLOAT2& position)
{
    position_ = position;
}

void TransformComponent::SetRotation(const float rotation)
{
    rotation_ = rotation;
}

void TransformComponent::SetScale(const XMFLOAT2& scale)
{
    scale_ = scale;
}

void TransformComponent::Move(const float x, const float y)
{
    position_.x += x;
    position_.y += y;
}

void TransformComponent::IncrementSpeed(const float x, const float y)
{
    velocityX_ += x;
    velocityY_ += y;
}

void TransformComponent::SetSpeed(const float x, const float y)
{
    velocityX_ = x;
    velocityY_ = y;
}

void TransformComponent::Update(float dt)
{

    //Move speed * time past since last frame to keep movement linear
    //Move(speedX_ * dt, speedY_ * dt);

    //speedX_ = 0.0f;
    //speedY_ = 0.0f;
}