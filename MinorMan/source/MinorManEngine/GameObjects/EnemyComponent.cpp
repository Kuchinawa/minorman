#include "EnemyComponent.h"

#include "CollisionComponent.h"
#include "StatusComponent.h"
#include "HeroStatusComponent.h"

#include "..\Physics\Collision.h"

using namespace MinorManEngine;

EnemyComponent::EnemyComponent(const std::shared_ptr<GameObject>& parent, const DirectX::XMFLOAT2 moveSpeed, const bool damagesOnTouch) : GOComponent()
{
    parent_ = parent;
    moveSpeed_ = moveSpeed; //Always go left first
    orientation_ = false;
    damagesOnTouch_ = damagesOnTouch;
    despawnTimer_ = 0.0f;

}

void EnemyComponent::Update(const float dt)
{
    if (despawnTimer_ > 0)
    {
        despawnTimer_ -= dt;
    }
}

void EnemyComponent::UpdateAI(const float dt, GameObject* player)
{
    if (damagesOnTouch_ && parent_.lock()->GetComponent<StatusComponent>()->IsAlive())
    {
        if (Collision::GJK2DCollision(parent_.lock()->GetComponent<CollisionComponent>()->GetBoundingBox(), player->GetComponent<CollisionComponent>()->GetBoundingBox()))
        {
            //Hit! Do damage
            player->GetComponent<HeroStatusComponent>()->TakeDamage(parent_.lock()->GetComponent<StatusComponent>()->GetAttackPower());
        }
    }
}