#include "HeroControlComponent.h"

#include <DirectXMath.h>

#include "CollisionComponent.h"
#include "TransformComponent.h"

#include "..\Config.h"

#include "..\Physics\AABB.h"
#include "..\Physics\Collision.h"

using namespace DirectX;
using namespace MinorManEngine;

HeroControlComponent::HeroControlComponent(const std::shared_ptr<GameObject>& parent) : GOComponent()
{
    parent_ = parent;
    SetState(std::unique_ptr<HeroState>(new HeroStateIdle()));

    blinkTransparency = 1.0f;

    onGround = false;
    orientation = true;
    speedPerSec = Config::heroSpeed;
    jumpSpeedY = Config::heroJumpSpeedY;
    chargeSpeedX = Config::heroChargeSpeed;
    hitCeiling = false;
}

void HeroControlComponent::HandleInput(
    const float dt,
    GameObject* hero,
    std::shared_ptr<Dx11Context> dx11Context,
    std::shared_ptr<SpriteRenderer> spriteRenderer,
    std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
    InputContext &inputContext,
    const TileRenderer &level,
    std::vector<std::shared_ptr<GameObject>> &enemies)
{
    //Update the control state
    currentState_->HandleInput(dt, hero, this, dx11Context, spriteRenderer, textures, inputContext, enemies);

    //Check for collisions

    XMVECTOR playerPos = parent_.lock()->GetComponent<TransformComponent>()->GetPosition();
    XMVECTOR playerVelocity = parent_.lock()->GetComponent<TransformComponent>()->GetVelocity() * dt;

    //Where are we next frame?
    XMVECTOR predictedPos = playerPos + (parent_.lock()->GetComponent<TransformComponent>()->GetVelocity() * dt);

    //Find the min / max
    XMVECTOR min = XMVectorMin(playerPos, predictedPos);
    XMVECTOR max = XMVectorMax(playerPos, predictedPos);

    //Extend by the player size
    min += parent_.lock()->GetComponent<CollisionComponent>()->GetMinExtend();
    max += parent_.lock()->GetComponent<CollisionComponent>()->GetMaxExtend();

    //Extend a bit extra
    XMVECTOR expand = XMLoadFloat2(new XMFLOAT2(5.0f, 5.0f));
    min -= expand;
    max += expand;


    int minTileX = level.WorldCoordsToTileCoordsX(XMVectorGetX(min));
    int minTileY = level.WorldCoordsToTileCoordsY(XMVectorGetY(min));

    //Round up
    int maxTileX = ceilf(level.WorldCoordsToTileCoordsX(XMVectorGetX(max)));
    int maxTileY = ceilf(level.WorldCoordsToTileCoordsY(XMVectorGetY(max)));

    XMVECTOR correction = XMLoadFloat2(new XMFLOAT2(0.0f, 0.0f));

    bool hitGround = false;

    for (int x = minTileX; x < maxTileX; x++)
    {
        for (int y = minTileY; y < maxTileY; y++)
        {
            if (!(x < 0) && !(y < 0) && !(y >= level.tiles.size()) && !(x >= level.tiles.at(0).size()) && level.tiles.at(y).at(x) != nullptr)
            {
                //Get player bounding box
                AABB p = parent_.lock()->GetComponent<CollisionComponent>()->aABB;
                p.SetCenter(playerPos);

                //Get tile bounding box
                AABB t = level.tiles.at(y).at(x)->GetComponent<CollisionComponent>()->aABB;
                t.SetCenter(level.tiles.at(y).at(x)->GetComponent<TransformComponent>()->GetPosition());

                float distance;
                XMVECTOR normal;
                XMVECTOR point;

                //Get collision data
                Collision::AABBvsAABB(p, t, distance, normal, point);

                //Get next coord, to check for internal edges
                int nextTileX = x + XMVectorGetX(normal);
                int nextTileY = y + XMVectorGetY(normal);

                if (!(nextTileX < 0) && !(nextTileY < 0) && !(nextTileY >= level.tiles.size()) && !(nextTileX >= level.tiles.at(0).size()))
                {
                    if (level.tiles.at(nextTileY).at(nextTileX) == nullptr)
                    {
                        float separation = fmax(distance, 0); // > 0
                        float penetration = fmin(distance, 0); // < 0

                        //Calculate the normal velocity required to hit the surface
                        float nv = XMVectorGetX(XMVector2Dot(playerVelocity, normal)) + separation / dt;

                        //Accumulate the penetration correction
                        correction -= normal * penetration;

                        //Do they touch?
                        if (nv < 0)
                        {
                            //Remove the normal velocity
                            playerVelocity -= normal * nv;

                            //Ground?
                            if (XMVectorGetY(normal) > 0)
                            {
                                parent_.lock()->GetComponent<HeroControlComponent>()->onGround = true;
                                hitGround = true;
                            } //Ceiling?
                            else if (XMVectorGetY(normal) < 0)
                            {
                                parent_.lock()->GetComponent<HeroControlComponent>()->hitCeiling = true;
                            }
                        }/*
                         else if (!hitGround)
                         {
                         player_->GetComponent<HeroControlComponent>()->onGround = false;
                         }*/
                    }
                }
            }
        }
    }

    playerVelocity += correction * dt;

    parent_.lock()->GetComponent<TransformComponent>()->Move(XMVectorGetX(playerVelocity), XMVectorGetY(playerVelocity));
    parent_.lock()->GetComponent<TransformComponent>()->SetSpeed(0.0f, 0.0f);
}

void HeroControlComponent::SetState(std::unique_ptr<HeroState> newState)
{
    currentState_ = std::move(newState);
}

void HeroControlComponent::Update(const float dt)
{

}