#include "WalkCannonEnemyComponent.h"

#include "CollisionComponent.h"
#include "RenderGOComponent.h"
#include "StatusComponent.h"
#include "TransformComponent.h"
#include "ProjectileEnemyComponent.h"

#include "..\Graphics\SpriteFileDescriptions.h"

#include "..\Physics\Collision.h"

#include "..\Config.h"

#include <DirectXMath.h>

using namespace MinorManEngine;
using namespace DirectX;

WalkCannonEnemyComponent::WalkCannonEnemyComponent(const std::shared_ptr<GameObject>& parent, const DirectX::XMFLOAT2 moveSpeed, const bool damagesOnTouch) : EnemyComponent(parent, moveSpeed, damagesOnTouch)
{
    attackCooldown_ = attackCooldownStart_;
    isAttacking_ = false;
}

void WalkCannonEnemyComponent::UpdateAI(
    const float dt,
    GameObject* player,
    std::shared_ptr<Dx11Context> &dx11Context,
    const std::shared_ptr<SpriteRenderer> &spriteRenderer,
    std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
    std::vector<std::shared_ptr<GameObject>> &bulletList)
{
    EnemyComponent::UpdateAI(dt, player);

    //Currently Attacking?
    if (isAttacking_)
    {   //Attack animation finished?
        if (parent_.lock()->GetComponent<RenderGOComponent>()->GetSprite()->AnimationFinished())
        {
            isAttacking_ = false;
            std::shared_ptr<Sprite> walkSprite(new Sprite(
                dx11Context,
                spriteRenderer,
                textures[SpriteFileDescriptions::WalkCannonWalking.fileName],
                SpriteFileDescriptions::WalkCannonWalking,
                EnemyComponent::orientation_));

            parent_.lock()->GetComponent<RenderGOComponent>()->SetSprite(walkSprite);
        }
    }
    else
    {
        XMVECTOR position = EnemyComponent::parent_.lock()->GetComponent<TransformComponent>()->GetPosition();
        XMVECTOR velocity = XMLoadFloat2(new XMFLOAT2(EnemyComponent::moveSpeed_.x * dt, EnemyComponent::moveSpeed_.y * dt));


        //On cooldown?
        if (attackCooldown_ > 0.0f)
        {
            attackCooldown_ -= dt;
        }
        else
        {
            //Check if the player is within the attack radius
            bool withinAttackRadius = XMVectorGetX(XMVector2Length(player->GetComponent<TransformComponent>()->GetPosition() - position)) < attackRange_;
            //Check if its facing the player
            bool facingPlayer = Collision::MovingTowardsX(parent_.lock()->GetComponent<TransformComponent>()->GetPosition(), velocity, player->GetComponent<TransformComponent>()->GetPosition());

            //Attackable?
            if (withinAttackRadius && facingPlayer && EnemyComponent::parent_.lock()->GetComponent<StatusComponent>()->IsAlive())
            {
                if (!isAttacking_)
                {
                    //Attack!
                    isAttacking_ = true;
                    std::shared_ptr<Sprite> attackSprite(new Sprite(
                        dx11Context,
                        spriteRenderer,
                        textures[SpriteFileDescriptions::WalkCannonAttacking.fileName],
                        SpriteFileDescriptions::WalkCannonAttacking,
                        EnemyComponent::orientation_,
                        false));

                    parent_.lock()->GetComponent<RenderGOComponent>()->SetSprite(attackSprite);
                    attackCooldown_ = attackCooldownStart_;

                    SpawnBullet(spriteRenderer, dx11Context, textures, bulletList);
                }
            }
        }

        if (!isAttacking_)
        {
            //Check if it hit one of it patrol points, if so reverse direction
            std::vector<XMVECTOR> bb = EnemyComponent::parent_.lock()->GetComponent<CollisionComponent>()->GetBoundingBox();

            for (int i = 0; i < patrolPoints.size(); i++)
            {
                std::vector<XMVECTOR> patrolPoint = { XMLoadFloat2(&patrolPoints.at(i)) };

                if (Collision::GJK2DCollision(bb, { XMLoadFloat2(&patrolPoints.at(i)) }))
                {
                    if (Collision::MovingTowardsX(position, velocity, XMLoadFloat2(&patrolPoints.at(i))))
                    {
                        EnemyComponent::moveSpeed_.x *= -1;
                        EnemyComponent::orientation_ = !EnemyComponent::orientation_;
                        EnemyComponent::parent_.lock()->GetComponent<RenderGOComponent>()->SetHorizontalOrientation(EnemyComponent::orientation_);
                    }
                }
            }

            EnemyComponent::parent_.lock()->GetComponent<TransformComponent>()->Move(EnemyComponent::moveSpeed_.x * dt, EnemyComponent::moveSpeed_.y * dt);
        }
    }

}

void WalkCannonEnemyComponent::AddPatrolPoint(const DirectX::XMFLOAT2 patrolPoint)
{
    patrolPoints.push_back(patrolPoint);
}

void WalkCannonEnemyComponent::SpawnBullet(
    const std::shared_ptr<SpriteRenderer> &spriteRenderer,
    std::shared_ptr<Dx11Context> &dx11Context,
    std::unordered_map < std::string, std::shared_ptr < Texture >> &textures,
    std::vector<std::shared_ptr<GameObject>> &bulletList)
{
    //Create enemy (Bullet)
    std::shared_ptr<GameObject> bullet = std::make_shared<GameObject>();

    //Spawn location
    XMVECTOR position = EnemyComponent::parent_.lock()->GetComponent<TransformComponent>()->GetPosition();

    float adjustX = 14.0f;
    float adjustY = 12.0f;

    float speedX = -224.0f;

    if (!EnemyComponent::orientation_)
    {
        adjustX *= -1;
        speedX *= -1;
    }

    std::unique_ptr<GOComponent> bulletTransformComp(new TransformComponent(XMVectorGetX(position) + adjustX, XMVectorGetY(position) + adjustY));
    bullet->AddComponent(std::move(bulletTransformComp));

    //Collision component
    std::unique_ptr<GOComponent> bulletCollisionComp(new CollisionComponent(bullet, std::vector<XMFLOAT2>() = { { -5.0f, 5.0f }, { 5.0f, 5.0f }, { 5.0f, -5.0f }, { -5.0f, -5.0f } }, XMFLOAT2(8.0f, 8.0f), XMFLOAT2(5.0f, 5.0f)));
    bullet->AddComponent(std::move(bulletCollisionComp));

    //Rendering
    std::shared_ptr<Sprite> bulletSprite(new Sprite(
        dx11Context,
        spriteRenderer,
        textures[SpriteFileDescriptions::WalkCannonBullet.fileName],
        SpriteFileDescriptions::WalkCannonBullet,
        false));

    std::unique_ptr<GOComponent> bulletRenderComp(new RenderGOComponent(bullet, dx11Context, spriteRenderer, bulletSprite));
    bullet->AddComponent(std::move(bulletRenderComp));

    //Status
    std::unique_ptr<StatusComponent> statusComp(new StatusComponent(10, Config::walkCannonProjectileDamage, true));
    bullet->AddComponent(std::move(statusComp));

    //AI
    std::unique_ptr<EnemyComponent> bulletEnemyComp(new ProjectileEnemyComponent(bullet, DirectX::XMFLOAT2(-speedX, 0.0f), 3.0f, true));
    bullet->AddComponent(std::move(bulletEnemyComp));

    bullet->Initialize();

    bulletList.push_back(bullet);
}