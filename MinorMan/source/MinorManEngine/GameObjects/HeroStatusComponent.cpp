#include "HeroStatusComponent.h"

#include "..\Config.h"

#include "RenderGOComponent.h"
#include "HeroControlComponent.h"

using namespace MinorManEngine;

HeroStatusComponent::HeroStatusComponent(const std::shared_ptr<GameObject>& parent, const int baseHealth, const int attackPower, const bool invulnerable) : StatusComponent(baseHealth, attackPower, invulnerable)
{
    parent_ = parent;
    tempInvul_ = false;
    tempInvulDur_ = 0.0f;
}

void HeroStatusComponent::TakeDamage(const int damage)
{
    if (!tempInvul_)
    {
        health_ -= damage;

        if (health_ <= 0)
        {
            health_ = 0;
            Kill();
        }
        else
        {
            //Go temporary invulnerable and start blinking
            tempInvul_ = true;
            tempInvulDur_ = Config::heroTempInvulDur;
            parent_.lock()->GetComponent<RenderGOComponent>()->GetSprite()->SetTransparency(0.5f);
            parent_.lock()->GetComponent<HeroControlComponent>()->blinkTransparency = 0.5f;
        }
    }
}

void HeroStatusComponent::Update(const float dt)
{
    if (tempInvul_)
    {
        tempInvulDur_ -= dt;
        
        //Temporary invulnerability over?
        if (tempInvulDur_ <= 0.0f)
        {
            tempInvul_ = false;
            parent_.lock()->GetComponent<RenderGOComponent>()->GetSprite()->SetTransparency(1.0f);
            parent_.lock()->GetComponent<HeroControlComponent>()->blinkTransparency = 1.0f;
        }
        else
        {
            //Blink
            if (fmod(tempInvulDur_, Config::heroInvulBlinkInterval * 2) > Config::heroInvulBlinkInterval)
            {
                parent_.lock()->GetComponent<RenderGOComponent>()->GetSprite()->SetTransparency(0.5f);
                parent_.lock()->GetComponent<HeroControlComponent>()->blinkTransparency = 0.5f;
            }
            else
            {
                parent_.lock()->GetComponent<RenderGOComponent>()->GetSprite()->SetTransparency(1.0f);
                parent_.lock()->GetComponent<HeroControlComponent>()->blinkTransparency = 1.0f;
            }
        }
    }
}