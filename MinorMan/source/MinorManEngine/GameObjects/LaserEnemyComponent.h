#pragma once

#include "EnemyComponent.h"

namespace MinorManEngine
{
    class LaserEnemyComponent : public EnemyComponent
    {
    public:
        LaserEnemyComponent(const std::shared_ptr<GameObject>& parent, const std::shared_ptr<GameObject>& cannon, const bool damagesOnTouch = true);

        void UpdateAI(const float dt, GameObject* player);

    private:

        std::weak_ptr<GameObject> cannon_;
    };
}