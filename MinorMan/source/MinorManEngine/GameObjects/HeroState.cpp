#include "HeroState.h"

#include "CollisionComponent.h"
#include "HeroControlComponent.h"
#include "HeroStatusComponent.h"
#include "ProjectileEnemyComponent.h"
#include "RenderGOComponent.h"
#include "StatusComponent.h"
#include "TransformComponent.h"

#include "..\Config.h"

#include "..\Graphics\Sprite.h"
#include "..\Graphics\SpriteFileDescriptions.h"

#include "..\Physics\Collision.h"

using namespace DirectX;
using namespace MinorManEngine;

void HeroStateIdle::HandleInput(
    const float dt,
    GameObject* hero,
    HeroControlComponent* context,
    std::shared_ptr<Dx11Context>& dx11Context,
    std::shared_ptr<SpriteRenderer>& spriteRenderer,
    std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
    InputContext& inputContext,
    std::vector<std::shared_ptr<GameObject>>& enemies)
{
    //Jump?
    if (inputContext.KeyPressed(DIK_SPACE))
    {
        std::shared_ptr<Sprite> jumpSprite(new Sprite(
            dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::HeroJump.fileName],
            SpriteFileDescriptions::HeroJump,
            context->orientation,
            true,
            false,
            context->blinkTransparency));

        hero->GetComponent<RenderGOComponent>()->SetSprite(jumpSprite);
        context->SetState(std::unique_ptr<HeroState>(new HeroStateJumping(false)));
        context->onGround = false;
        return;
    }
    else if (!context->onGround)
    {
        std::shared_ptr<Sprite> jumpSprite(new Sprite(
            dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::HeroJump.fileName],
            SpriteFileDescriptions::HeroJump,
            context->orientation,
            true,
            true,
            context->blinkTransparency));

        hero->GetComponent<RenderGOComponent>()->SetSprite(jumpSprite);
        context->SetState(std::unique_ptr<HeroState>(new HeroStateJumping(true)));
        return;
    }

    //Charge?
    if (inputContext.KeyPressed(DIK_S))
    {
        std::shared_ptr<Sprite> chargeSprite(new Sprite(
            dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::HeroCharge.fileName],
            SpriteFileDescriptions::HeroCharge,
            context->orientation,
            true,
            false,
            context->blinkTransparency));

        hero->GetComponent<RenderGOComponent>()->SetSprite(chargeSprite);
        context->SetState(std::unique_ptr<HeroState>(new HeroStateCharging(Config::heroChargeDuration)));

        return;
    }


    //Running Right
    if (inputContext.KeyDown(DIK_RIGHT))
    {
        context->orientation = true;
        std::shared_ptr<Sprite> runningSprite(new Sprite(
            dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::HeroRunning.fileName],
            SpriteFileDescriptions::HeroRunning,
            context->orientation,
            true,
            false,
            context->blinkTransparency));

        hero->GetComponent<RenderGOComponent>()->SetSprite(runningSprite);
        context->SetState(std::unique_ptr<HeroState>(new HeroStateRunning()));
    }

    //Running Left
    if (inputContext.KeyDown(DIK_LEFT))
    {
        context->orientation = false;
        std::shared_ptr<Sprite> runningSprite(new Sprite(
            dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::HeroRunning.fileName],
            SpriteFileDescriptions::HeroRunning,
            context->orientation,
            true,
            false,
            context->blinkTransparency));

        hero->GetComponent<RenderGOComponent>()->SetSprite(runningSprite);
        context->SetState(std::unique_ptr<HeroState>(new HeroStateRunning()));
    }

    //Attack?
    if (inputContext.KeyPressed(DIK_A))
    {
        std::shared_ptr<Sprite> attackSprite(new Sprite(
            dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::HeroAttack.fileName],
            SpriteFileDescriptions::HeroAttack,
            context->orientation,
            true,
            false,
            context->blinkTransparency));

        hero->GetComponent<RenderGOComponent>()->SetSprite(attackSprite);
        context->SetState(std::unique_ptr<HeroState>(new HeroStateAttacking()));
    }

    //Gravity
    hero->GetComponent<TransformComponent>()->IncrementSpeed(0.0f, Config::gravity);
}

void HeroStateRunning::HandleInput(
    const float dt,
    GameObject* hero,
    HeroControlComponent* context,
    std::shared_ptr<Dx11Context>& dx11Context,
    std::shared_ptr<SpriteRenderer>& spriteRenderer,
    std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
    InputContext& inputContext,
    std::vector<std::shared_ptr<GameObject>>& enemies)
{
    //Jump?
    if (inputContext.KeyPressed(DIK_SPACE))
    {
        std::shared_ptr<Sprite> jumpSprite(new Sprite(
            dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::HeroJump.fileName],
            SpriteFileDescriptions::HeroJump,
            context->orientation,
            true,
            false,
            context->blinkTransparency));

        hero->GetComponent<RenderGOComponent>()->SetSprite(jumpSprite);
        context->SetState(std::unique_ptr<HeroState>(new HeroStateJumping(false)));
        context->onGround = false;
        return;
    }
    else if (!context->onGround)
    {
        std::shared_ptr<Sprite> jumpSprite(new Sprite(
            dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::HeroJump.fileName],
            SpriteFileDescriptions::HeroJump,
            context->orientation,
            true,
            true,
            context->blinkTransparency));

        hero->GetComponent<RenderGOComponent>()->SetSprite(jumpSprite);
        context->SetState(std::unique_ptr<HeroState>(new HeroStateJumping(true)));
        return;
    }

    //Still running?
    if (inputContext.KeyDown(DIK_RIGHT))
    {
        hero->GetComponent<TransformComponent>()->IncrementSpeed(context->speedPerSec, 0.0f);
    }
    else if (inputContext.KeyUp(DIK_RIGHT))
    {
        std::shared_ptr<Sprite> idleSprite(new Sprite(
            dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::HeroIdle.fileName],
            SpriteFileDescriptions::HeroIdle,
            context->orientation,
            true,
            false,
            context->blinkTransparency));

        hero->GetComponent<RenderGOComponent>()->SetSprite(idleSprite);
        context->SetState(std::unique_ptr<HeroState>(new HeroStateIdle()));
    }

    //Still running?
    if (inputContext.KeyDown(DIK_LEFT))
    {
        hero->GetComponent<TransformComponent>()->IncrementSpeed(-context->speedPerSec, 0.0f);
    }
    else if (inputContext.KeyUp(DIK_LEFT))
    {
        std::shared_ptr<Sprite> idleSprite(new Sprite(
            dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::HeroIdle.fileName],
            SpriteFileDescriptions::HeroIdle,
            context->orientation,
            true,
            false,
            context->blinkTransparency));

        hero->GetComponent<RenderGOComponent>()->SetSprite(idleSprite);
        context->SetState(std::unique_ptr<HeroState>(new HeroStateIdle()));
    }

    //Charge?
    if (inputContext.KeyPressed(DIK_S))
    {
        std::shared_ptr<Sprite> chargeSprite(new Sprite(
            dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::HeroCharge.fileName],
            SpriteFileDescriptions::HeroCharge,
            context->orientation,
            true,
            false,
            context->blinkTransparency));

        hero->GetComponent<RenderGOComponent>()->SetSprite(chargeSprite);
        context->SetState(std::unique_ptr<HeroState>(new HeroStateCharging(Config::heroChargeDuration)));

        return;
    }

    //Attack?
    if (inputContext.KeyPressed(DIK_A))
    {
        std::shared_ptr<Sprite> attackSprite(new Sprite(
            dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::HeroAttack.fileName],
            SpriteFileDescriptions::HeroAttack,
            context->orientation,
            true,
            false,
            context->blinkTransparency));

        hero->GetComponent<RenderGOComponent>()->SetSprite(attackSprite);
        context->SetState(std::unique_ptr<HeroState>(new HeroStateAttacking()));
    }



    //Gravity
    hero->GetComponent<TransformComponent>()->IncrementSpeed(0.0f, Config::gravity);
}

HeroStateJumping::HeroStateJumping(const bool startFalling)
{
    if (startFalling)
    {
        jumpTimer_ = 0.0f;
    }
    else
    {
        jumpTimer_ = 0.5f; //Seconds
    }

}

void HeroStateJumping::HandleInput(
    const float dt,
    GameObject* hero,
    HeroControlComponent* context,
    std::shared_ptr<Dx11Context>& dx11Context,
    std::shared_ptr<SpriteRenderer>& spriteRenderer,
    std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
    InputContext& inputContext,
    std::vector<std::shared_ptr<GameObject>>& enemies)
{
    if (context->onGround)
    {
        std::shared_ptr<Sprite> idleSprite(new Sprite(
            dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::HeroIdle.fileName],
            SpriteFileDescriptions::HeroIdle,
            context->orientation,
            true,
            false,
            context->blinkTransparency));

        hero->GetComponent<RenderGOComponent>()->SetSprite(idleSprite);
        context->SetState(std::unique_ptr<HeroState>(new HeroStateIdle()));
    }

    if (inputContext.KeyDown(DIK_RIGHT))
    {
        context->orientation = true;
        hero->GetComponent<RenderGOComponent>()->SetHorizontalOrientation(context->orientation);
        hero->GetComponent<TransformComponent>()->IncrementSpeed(context->speedPerSec, 0.0f);
    }

    if (inputContext.KeyDown(DIK_LEFT))
    {
        context->orientation = false;
        hero->GetComponent<RenderGOComponent>()->SetHorizontalOrientation(context->orientation);
        hero->GetComponent<TransformComponent>()->IncrementSpeed(-context->speedPerSec, 0.0f);
    }

    jumpTimer_ -= dt;

    if (context->hitCeiling)
    {
        jumpTimer_ = 0.0f;
        context->hitCeiling = false;
    }

    if (jumpTimer_ > 0.0f)
    {
        hero->GetComponent<TransformComponent>()->IncrementSpeed(0.0f, context->jumpSpeedY);
    }
    else
    {
        hero->GetComponent<TransformComponent>()->IncrementSpeed(0.0f, Config::gravity);
    }

}

HeroStateAttacking::HeroStateAttacking()
{
    cooldownRemaining_ = attackCooldown_;
    timePassed_ = 0.0f;
    didDamage_ = false;

    swordCollisionBox_.reserve(6);
    swordCollisionBox_.push_back(XMFLOAT2(19, 38));
    swordCollisionBox_.push_back(XMFLOAT2(46, 29));
    swordCollisionBox_.push_back(XMFLOAT2(64, 6));
    swordCollisionBox_.push_back(XMFLOAT2(64, -15));
    swordCollisionBox_.push_back(XMFLOAT2(50, -21));
    swordCollisionBox_.push_back(XMFLOAT2(29, -12));
}

void HeroStateAttacking::HandleInput(
    const float dt,
    GameObject* hero,
    HeroControlComponent* context,
    std::shared_ptr<Dx11Context>& dx11Context,
    std::shared_ptr<SpriteRenderer>& spriteRenderer,
    std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
    InputContext& inputContext,
    std::vector<std::shared_ptr<GameObject>>& enemies)
{
    cooldownRemaining_ -= dt;
    timePassed_ += dt;

    //Did this attack do damage yet and are we at the point where it should?
    if (!didDamage_ && timePassed_ >= damageTimePoint_)
    {
        std::vector<XMVECTOR> swordCollisionBox;

        int direction = 1;
        if (!context->orientation)
        {
            direction = -1;
        }

        //Adjust position of the collision box according to the hero's position and orientation
        for (int i = 0; i < swordCollisionBox_.size(); i++)
        {
            XMVECTOR collisionPoint = XMLoadFloat2(&swordCollisionBox_.at(i));
            swordCollisionBox.push_back(XMVectorSetX(collisionPoint, XMVectorGetX(collisionPoint) * direction) + hero->GetComponent<TransformComponent>()->GetPosition());
        }

        for (int i = 0; i < enemies.size(); i++)
        {
            //Did we hit the enemy?
            if (Collision::GJK2DCollision(swordCollisionBox, enemies.at(i)->GetComponent<CollisionComponent>()->GetBoundingBox()))
            {
                if (!enemies.at(i)->GetComponent<StatusComponent>()->IsInvulnerable())
                {
                    //Hit! Do damage
                    enemies.at(i)->GetComponent<StatusComponent>()->TakeDamage(hero->GetComponent<HeroStatusComponent>()->GetAttackPower());
                }
            }
        }

        didDamage_ = true;
    }

    //Attack complete? return to idle state
    if (hero->GetComponent<RenderGOComponent>()->GetSprite()->AnimationFinished())
    {
        std::shared_ptr<Sprite> idleSprite(new Sprite(
            dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::HeroIdle.fileName],
            SpriteFileDescriptions::HeroIdle,
            context->orientation,
            true,
            false,
            context->blinkTransparency));

        hero->GetComponent<RenderGOComponent>()->SetSprite(idleSprite);
        context->SetState(std::unique_ptr<HeroState>(new HeroStateIdle()));
    }

    //Attack?
    if (inputContext.KeyPressed(DIK_A) && cooldownRemaining_ <= 0.0f)
    {
        std::shared_ptr<Sprite> attackSprite(new Sprite(
            dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::HeroAttack.fileName],
            SpriteFileDescriptions::HeroAttack,
            context->orientation,
            true,
            false,
            context->blinkTransparency));

        hero->GetComponent<RenderGOComponent>()->SetSprite(attackSprite);
        context->SetState(std::unique_ptr<HeroState>(new HeroStateAttacking()));
    }
}

HeroStateCharging::HeroStateCharging(const float duration)
{
    duration_ = duration;
    timePassed_ = 0.0f;
}


void HeroStateCharging::HandleInput(
    const float dt,
    GameObject* hero,
    HeroControlComponent* context,
    std::shared_ptr<Dx11Context>& dx11Context,
    std::shared_ptr<SpriteRenderer>& spriteRenderer,
    std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
    InputContext& inputContext,
    std::vector<std::shared_ptr<GameObject>>& enemies)
{
    timePassed_ += dt;

    //Still charging?
    if (timePassed_ < duration_)
    {
        float speedX = context->chargeSpeedX;

        if (!context->orientation)
        {
            speedX *= -1;
        }

        hero->GetComponent<TransformComponent>()->IncrementSpeed(speedX, 0.0f);
    }
    else
    {
        std::shared_ptr<Sprite> idleSprite(new Sprite(
            dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::HeroIdle.fileName],
            SpriteFileDescriptions::HeroIdle,
            context->orientation,
            true,
            false,
            context->blinkTransparency));

        hero->GetComponent<RenderGOComponent>()->SetSprite(idleSprite);
        context->SetState(std::unique_ptr<HeroState>(new HeroStateIdle()));
    }
}