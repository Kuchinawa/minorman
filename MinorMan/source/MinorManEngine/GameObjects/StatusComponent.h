#pragma once

#include "GOComponent.h"

namespace MinorManEngine
{
    class StatusComponent : public GOComponent
    {
    public:
        StatusComponent(const int baseHealth, const int attackPower, const bool invulnerable = false);

        int GetHealth() const;
        void TakeDamage(const int damage);

        int GetAttackPower() const;
        
        bool IsAlive() const;
        void Kill();
        
        bool IsInvulnerable() const;

    protected:

        int health_;
        int attackPower_;

        bool alive_;
        bool invulnerable_;

    };
}