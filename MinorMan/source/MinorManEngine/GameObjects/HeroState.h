#pragma once

#include "GameObject.h"

#include "..\Graphics\Dx11Context.h"
#include "..\Graphics\SpriteRenderer.h"
#include "..\Graphics\Texture.h"


#include "..\Input\InputContext.h"

#include <DirectXMath.h>

namespace MinorManEngine
{
    class HeroControlComponent;
}


namespace MinorManEngine
{
    class HeroState
    {
    public:
        virtual void HandleInput(
            const float dt,
            GameObject* hero,
            HeroControlComponent* context,
            std::shared_ptr<Dx11Context>& dx11Context,
            std::shared_ptr<SpriteRenderer>& spriteRenderer,
            std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
            InputContext& inputContext,
            std::vector<std::shared_ptr<GameObject>>& enemies
            ) = 0;
    };

    class HeroStateIdle : public HeroState
    {
    public:
        void HandleInput(
            const float dt,
            GameObject* hero,
            HeroControlComponent* context,
            std::shared_ptr<Dx11Context>& dx11Context,
            std::shared_ptr<SpriteRenderer>& spriteRenderer,
            std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
            InputContext& inputContext,
            std::vector<std::shared_ptr<GameObject>>& enemies);
    };

    class HeroStateRunning : public HeroState
    {
    public:
        void HandleInput(
            const float dt,
            GameObject* hero,
            HeroControlComponent* context,
            std::shared_ptr<Dx11Context>& dx11Context,
            std::shared_ptr<SpriteRenderer>& spriteRenderer,
            std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
            InputContext& inputContext,
            std::vector<std::shared_ptr<GameObject>>& enemies);
    };

    class HeroStateJumping : public HeroState
    {
    public:
        HeroStateJumping(const bool startFalling);
        void HandleInput(
            const float dt,
            GameObject* hero,
            HeroControlComponent* context,
            std::shared_ptr<Dx11Context>& dx11Context,
            std::shared_ptr<SpriteRenderer>& spriteRenderer,
            std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
            InputContext& inputContext,
            std::vector<std::shared_ptr<GameObject>>& enemies);

    private:
        float jumpTimer_;
    };

    class HeroStateAttacking : public HeroState
    {
    public:
        HeroStateAttacking();
        void HandleInput(
            const float dt,
            GameObject* hero,
            HeroControlComponent* context,
            std::shared_ptr<Dx11Context>& dx11Context,
            std::shared_ptr<SpriteRenderer>& spriteRenderer,
            std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
            InputContext& inputContext,
            std::vector<std::shared_ptr<GameObject>>& enemies);

    private:
        std::vector<DirectX::XMFLOAT2> swordCollisionBox_;

        const float damageTimePoint_ = 0.145f;
        const float attackCooldown_ = 0.15f;

        float timePassed_;
        float cooldownRemaining_;

        bool didDamage_;


    };

    class HeroStateCharging : public HeroState
    {
    public:
        HeroStateCharging(const float duration);

        void HandleInput(
            const float dt,
            GameObject* hero,
            HeroControlComponent* context,
            std::shared_ptr<Dx11Context>& dx11Context,
            std::shared_ptr<SpriteRenderer>& spriteRenderer,
            std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
            InputContext& inputContext,
            std::vector<std::shared_ptr<GameObject>>& enemies);

    private:
        float duration_;
        float timePassed_;

    };
}