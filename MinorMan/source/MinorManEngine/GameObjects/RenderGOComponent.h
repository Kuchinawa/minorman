#pragma once

#include "GOComponent.h"
#include "GameObject.h"

#include "..\Graphics\Camera.h"
#include "..\Graphics\Dx11Context.h"
#include "..\Graphics\Sprite.h"
#include "..\Graphics\SpriteRenderer.h"

#include "..\GameObjects\TransformComponent.h"

#include <memory>

namespace MinorManEngine
{
    class RenderGOComponent : public GOComponent
    {
    public:
        RenderGOComponent(const std::shared_ptr<GameObject>& parent,
            const std::shared_ptr<Dx11Context>& directXContext,
            const std::shared_ptr<SpriteRenderer>& spriteRenderer,
            const std::shared_ptr<Sprite>& defaultSprite);

        ~RenderGOComponent();
        void Uninitialize();
        void Update(const float dt);
        void Render(const Camera &camera);

        void SetHorizontalOrientation(const bool right);
        void SetSprite(const std::shared_ptr<Sprite>& sprite);

        std::shared_ptr<Sprite> GetSprite();

    private:
        std::weak_ptr<GameObject> parent_;

        std::shared_ptr<Sprite> sprite_;
    };
}