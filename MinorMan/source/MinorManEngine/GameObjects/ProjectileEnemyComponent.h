#pragma once

#include "GameObject.h"
#include "EnemyComponent.h"

#include "..\Graphics\TileRenderer.h"

namespace MinorManEngine
{
    class ProjectileEnemyComponent : public EnemyComponent
    {
    public:
        ProjectileEnemyComponent(const std::shared_ptr<GameObject>& parent, const DirectX::XMFLOAT2 moveSpeed, const float ttl, const bool damagesOnTouch = true);

        void UpdateAI(const float dt, GameObject* player, TileRenderer &terrain);

    private:
        float ttl_;
    };
}