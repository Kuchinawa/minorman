#pragma once

#include <vector>

#include <DirectXMath.h>

#include "EnemyComponent.h"
#include "..\Graphics\Dx11Context.h"
#include "..\Graphics\SpriteRenderer.h"
#include "..\Graphics\Texture.h"

namespace MinorManEngine
{
    class WalkCannonEnemyComponent : public EnemyComponent
    {
    public:
        WalkCannonEnemyComponent(const std::shared_ptr<GameObject>& parent, const DirectX::XMFLOAT2 moveSpeed, const bool damagesOnTouch);

        void UpdateAI(
            const float dt,
            GameObject* player,
            std::shared_ptr<Dx11Context> &dx11Context,
            const std::shared_ptr<SpriteRenderer> &spriteRenderer,
            std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
            std::vector<std::shared_ptr<GameObject>> &bulletList);

        void AddPatrolPoint(const DirectX::XMFLOAT2 patrolPoint);

        void SpawnBullet(
            const std::shared_ptr<SpriteRenderer> &spriteRenderer,
            std::shared_ptr<Dx11Context> &dx11Context,
            std::unordered_map < std::string, std::shared_ptr < Texture >> &textures,
            std::vector<std::shared_ptr<GameObject>> &bulletList);

    private:

        const float attackCooldownStart_ = 2.0f;
        const float attackRange_ = 256.0f;

        float attackCooldown_;
        bool isAttacking_;

        //The patrol boundaries
        std::vector<DirectX::XMFLOAT2> patrolPoints;
    };
}