#include "StatusComponent.h"

using namespace MinorManEngine;

StatusComponent::StatusComponent(int baseHealth, int attackPower, bool invulnerable) : GOComponent()
{
    health_ = baseHealth;
    attackPower_ = attackPower;

    alive_ = true;
    invulnerable_ = invulnerable;
}

int StatusComponent::GetHealth() const
{
    return health_;
}

void StatusComponent::TakeDamage(int damage)
{
    if (!invulnerable_)
    {
        health_ -= damage;

        if (health_ <= 0)
        {
            Kill();
        }
    }
}

int StatusComponent::GetAttackPower() const
{
    return attackPower_;
}

bool StatusComponent::IsAlive() const
{
    return alive_;
}

void StatusComponent::Kill()
{
    alive_ = false;
}

bool StatusComponent::IsInvulnerable() const
{
    return invulnerable_;
}