#pragma once

#include <unordered_map>
#include <type_traits>
#include <memory>

#include "GOComponent.h"

namespace MinorManEngine
{
    class GameObject
    {
    public:
        GameObject()
        {

        }

        ~GameObject()
        {
        };

        void Initialize()
        {
            //Loop through all the attached components and call their init method
            for (auto &component : components_)
            {
                component.second->Initialize();
            }
        }

        void Uninitialize()
        {
            //Loop through all the attached components and call their uninitialize method
            for (auto &component : components_)
            {
                component.second->Uninitialize();
            }
        }

        void Update(const float dt)
        {
            //Loop through all the attached components and call their update method
            for (auto &component : components_)
            {
                component.second->Update(dt);
            }
        }

        //Add a component to the components_ uomap using the type as key
        void AddComponent(std::unique_ptr<GOComponent> component)
        {
            components_[&typeid(*component.get())] = std::move(component);
        }

        //Get the component based on type T
        template <typename T>
        T* GetComponent()
        {
            if (components_.count(&typeid(T)) != 0)
            {
                return static_cast<T*>(components_[&typeid(T)].get());
            }
            else
            {
                return nullptr;
            }
        }

    private:
        std::unordered_map<const std::type_info*, std::unique_ptr<GOComponent>> components_;
    };
}