#pragma once

namespace MinorManEngine
{
    class GOComponent
    {
    public:
        virtual bool Initialize(){ return true; };
        virtual void Uninitialize() {};
        virtual void Update(const float dt){};
    };
}