#include "LaserEnemyComponent.h"

#include "RenderGOComponent.h"
#include "StatusComponent.h"

using namespace MinorManEngine;

LaserEnemyComponent::LaserEnemyComponent(const std::shared_ptr<GameObject>& parent, const std::shared_ptr<GameObject>& cannon, const bool damagesOnTouch) : EnemyComponent(parent, DirectX::XMFLOAT2(0.0f, 0.0f), damagesOnTouch)
{
    cannon_ = cannon;
}

void LaserEnemyComponent::UpdateAI(const float dt, GameObject* player)
{
    EnemyComponent::UpdateAI(dt, player);

    if (parent_.lock()->GetComponent<RenderGOComponent>()->GetSprite()->AnimationFinished() || cannon_.expired())
    {
        //Kill when animation is finished
        parent_.lock()->GetComponent<StatusComponent>()->Kill();
    }
}