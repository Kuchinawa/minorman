#include "laserCannonComponent.h"

#include "..\Config.h"

#include "RenderGOComponent.h"
#include "CollisionComponent.h"
#include "StatusComponent.h"
#include "LaserEnemyComponent.h"

#include "..\Graphics\SpriteFileDescriptions.h"

using namespace MinorManEngine;
using namespace DirectX;

LaserCannonComponent::LaserCannonComponent(const std::shared_ptr<GameObject>& parent, const bool damagesOnTouch, const bool orientation) : EnemyComponent(parent, DirectX::XMFLOAT2(0.0f, 0.0f), damagesOnTouch)
{
    shooting_ = false;
    EnemyComponent::orientation_ = orientation;
}

void LaserCannonComponent::UpdateAI(const float dt,
    GameObject* player,
    std::shared_ptr<Dx11Context> &dx11Context,
    const std::shared_ptr<SpriteRenderer> &spriteRenderer,
    std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
    std::vector<std::shared_ptr<GameObject>> &bulletList)
{
    EnemyComponent::UpdateAI(dt, player);

    //When the shooting animation is finished switch to the idle/charging animation, and the reverse
    if (shooting_ && parent_.lock()->GetComponent<RenderGOComponent>()->GetSprite()->AnimationFinished())
    {
        shooting_ = false;

        std::shared_ptr<Sprite> chargingSprite(
            new Sprite(dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::laserCannon.fileName],
            SpriteFileDescriptions::laserCannon,
            EnemyComponent::orientation_));

        parent_.lock()->GetComponent<RenderGOComponent>()->SetSprite(chargingSprite);
    }
    else if (!shooting_ && parent_.lock()->GetComponent<RenderGOComponent>()->GetSprite()->AnimationFinished())
    {
        shooting_ = true;

        //Set the shooting sprite
        std::shared_ptr<Sprite> shootingSprite(
            new Sprite(dx11Context,
            spriteRenderer,
            textures[SpriteFileDescriptions::laserCannonShoot.fileName],
            SpriteFileDescriptions::laserCannonShoot,
            EnemyComponent::orientation_));

        parent_.lock()->GetComponent<RenderGOComponent>()->SetSprite(shootingSprite);

        for (int i = 0; i < 8; i++)
        {
            //Create enemy (laser)
            std::shared_ptr<GameObject> laser = std::make_shared<GameObject>();

            //Spawn location
            XMVECTOR position = EnemyComponent::parent_.lock()->GetComponent<TransformComponent>()->GetPosition();

            float adjustX = 52.0f + i * 32;
            float adjustY = 6.0f;

            if (!EnemyComponent::orientation_)
            {
                adjustX *= -1;
            }

            std::unique_ptr<GOComponent> laserTransformComp(new TransformComponent(XMVectorGetX(position) + adjustX, XMVectorGetY(position) + adjustY));
            laser->AddComponent(std::move(laserTransformComp));

            //Collision component
            std::unique_ptr<GOComponent> laserCollisionComp(new CollisionComponent(laser, std::vector<XMFLOAT2>() = { { -16.0f, 24.0f }, { -16.0f, 24.0f }, { 16.0f, -24.0f }, { -16.0f, -24.0f } }, XMFLOAT2(16.0f, 32.0f), XMFLOAT2(16.0f, 24.0f)));
            laser->AddComponent(std::move(laserCollisionComp));

            //Rendering
            std::shared_ptr<Sprite> laserSprite(new Sprite(
                dx11Context,
                spriteRenderer,
                textures[SpriteFileDescriptions::laser.fileName],
                SpriteFileDescriptions::laser,
                false));

            std::unique_ptr<GOComponent> laserRenderComp(new RenderGOComponent(laser, dx11Context, spriteRenderer, laserSprite));
            laser->AddComponent(std::move(laserRenderComp));

            //Status
            std::unique_ptr<StatusComponent> statusComp(new StatusComponent(10, Config::laserCannonDamage, true));
            laser->AddComponent(std::move(statusComp));

            //Laser logic
            std::unique_ptr<EnemyComponent> laserEnemyComp(new LaserEnemyComponent(laser, parent_.lock(), true));
            laser->AddComponent(std::move(laserEnemyComp));

            laser->Initialize();

            bulletList.push_back(laser);
        }
    }

}