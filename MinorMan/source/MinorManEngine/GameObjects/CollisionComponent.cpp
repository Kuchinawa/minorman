#include "CollisionComponent.h"

#include "TransformComponent.h"

using namespace DirectX;
using namespace MinorManEngine;

CollisionComponent::CollisionComponent(const std::shared_ptr<GameObject>& parent, std::vector<XMFLOAT2> boundingBox, XMFLOAT2 center, XMFLOAT2 halfExtends)
{
    parent_ = parent;
    SetBoundingBox(boundingBox);
    aABB = AABB(center, halfExtends);
}

std::vector<XMVECTOR> CollisionComponent::GetBoundingBox() const
{
    std::vector<XMVECTOR> boundingBox;

    for (XMFLOAT2 point : boundingBox_)
    {
        XMVECTOR realPoint = parent_.lock()->GetComponent<TransformComponent>()->GetPosition() + XMLoadFloat2(&point);
        boundingBox.push_back(realPoint);
    }

    return boundingBox;
}

void CollisionComponent::SetBoundingBox(std::vector<XMFLOAT2> boundingBox)
{
    boundingBox_ = boundingBox;
}


XMVECTOR CollisionComponent::GetMaxExtend()
{
    std::vector<XMVECTOR> boundingBox;
    for (XMFLOAT2 point : boundingBox_)
    {
        boundingBox.push_back(XMLoadFloat2(&point));
    }

    if (boundingBox.size() > 2)
    {
        XMVECTOR max;
        max = boundingBox.at(0);
        //Get the highest vector
        for (int i = 1; i < boundingBox.size(); i++)
        {
            max = XMVectorMax(max, boundingBox.at(i));
        }

        return max;
    }
    else if (boundingBox.size() == 2)
    {
        return XMVectorMax(boundingBox.at(0), boundingBox.at(1));
    }
    else
    {
        return boundingBox.at(0);
    }
}

XMVECTOR CollisionComponent::GetMinExtend()
{
    std::vector<XMVECTOR> boundingBox;
    for (XMFLOAT2 point : boundingBox_)
    {
        boundingBox.push_back(XMLoadFloat2(&point));
    }

    if (boundingBox.size() > 2)
    {
        XMVECTOR min;
        min = boundingBox.at(0);
        //Get the highest vector
        for (int i = 1; i < boundingBox.size(); i++)
        {
            min = XMVectorMin(min, boundingBox.at(i));
        }

        return min;
    }
    else if (boundingBox.size() == 2)
    {
        return XMVectorMin(boundingBox.at(0), boundingBox.at(1));
    }
    else
    {
        return boundingBox.at(0);
    }
}
