#pragma once

#include "StatusComponent.h"

#include "GameObject.h"

namespace MinorManEngine
{
    class HeroStatusComponent : public StatusComponent
    {
    public:
        HeroStatusComponent(const std::shared_ptr<GameObject>& parent, const int baseHealth, const int attackPower, const bool invulnerable = false);

        void TakeDamage(const int damage);

        void Update(const float dt);

    private:
        std::weak_ptr<GameObject> parent_;

        bool tempInvul_;
        float tempInvulDur_;
        

    };
}