#pragma once
#include <DirectXMath.h>
#include "..\GameObjects\GOComponent.h"
#pragma once

#include "..\Graphics\Dx11Context.h"
#include "..\Graphics\VertexPos.h"

#include <D3D11.h>
#include <D3DX11.h>
#include <DxErr.h>

#include <memory>
#include <string>
#include <vector>

namespace MinorManEngine
{
    class TransformComponent : public GOComponent
    {
    public:
        TransformComponent(const float x, const float y);
        virtual ~TransformComponent();

        bool Initialize();

        DirectX::XMMATRIX GetWorldMatrix();
        DirectX::XMVECTOR GetPosition();
        DirectX::XMVECTOR GetVelocity();

        float GetVelocityX();
        float GetVelocityY();

        void SetPosition(const DirectX::XMFLOAT2& position);
        void SetRotation(const float rotation);
        void SetScale(const DirectX::XMFLOAT2& scale);

        void Move(const float x, const float y);
        

        void IncrementSpeed(const float x, const float y);
        void SetSpeed(const float x, const float y);

        void Update(float dt);

    private:
        DirectX::XMFLOAT2 position_;
        float rotation_;
        DirectX::XMFLOAT2 scale_;

        float velocityX_;
        float velocityY_;
    };
}