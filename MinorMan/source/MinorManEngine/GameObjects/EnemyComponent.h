#pragma once

#include <memory>

#include "GameObject.h"
#include "GOComponent.h"

#include <DirectXMath.h>

namespace MinorManEngine
{
    class EnemyComponent : public GOComponent
    {
    public:
        EnemyComponent(const std::shared_ptr<GameObject>& parent, const DirectX::XMFLOAT2 moveSpeed, const bool damagesOnTouch);

        void Update(const float dt);

        void UpdateAI(const float dt, GameObject* player);

    protected:
        std::weak_ptr<GameObject> parent_;
        DirectX::XMFLOAT2 moveSpeed_;
        bool orientation_; //True = right
        bool damagesOnTouch_;

    private:

        float despawnTimer_;


    };
}