#include "ProjectileEnemyComponent.h"

#include "CollisionComponent.h"
#include "StatusComponent.h"
#include "TransformComponent.h"

#include "..\Physics\Collision.h"

using namespace MinorManEngine;
using namespace DirectX;

ProjectileEnemyComponent::ProjectileEnemyComponent(const std::shared_ptr<GameObject>& parent, const DirectX::XMFLOAT2 moveSpeed, const float ttl, const bool damagesOnTouch) : EnemyComponent(parent, moveSpeed, damagesOnTouch)
{
    ttl_ = ttl;
}

void ProjectileEnemyComponent::UpdateAI(const float dt, GameObject* player, TileRenderer &terrain)
{
    EnemyComponent::UpdateAI(dt, player);

    if (EnemyComponent::parent_.lock()->GetComponent<StatusComponent>()->IsAlive())
    {
        //Check alive timer
        ttl_ -= dt;
        if (ttl_ <= 0.0f)
        {
            //When expired despawn
            EnemyComponent::parent_.lock()->GetComponent<StatusComponent>()->Kill();
        }
        else
        {
            ////Check for collisions with the terrain

            XMVECTOR position = parent_.lock()->GetComponent<TransformComponent>()->GetPosition();

            //Where are we next frame?
            XMVECTOR predictedPos = position + (XMLoadFloat2(&moveSpeed_)* dt);
                //XMVectorSetX(position, XMVectorGetX(position) + EnemyComponent::moveSpeed_ * dt);

            //Find the min / max
            XMVECTOR min = XMVectorMin(position, predictedPos);
            XMVECTOR max = XMVectorMax(position, predictedPos);

            //Extend by the bullet size
            min += parent_.lock()->GetComponent<CollisionComponent>()->GetMinExtend();
            max += parent_.lock()->GetComponent<CollisionComponent>()->GetMaxExtend();

            //Extend a bit extra
            XMVECTOR expand = XMLoadFloat2(new XMFLOAT2(5.0f, 5.0f));
            min -= expand;
            max += expand;


            int minTileX = terrain.WorldCoordsToTileCoordsX(XMVectorGetX(min));
            int minTileY = terrain.WorldCoordsToTileCoordsY(XMVectorGetY(min));

            //Round up
            int maxTileX = ceilf(terrain.WorldCoordsToTileCoordsX(XMVectorGetX(max)));
            int maxTileY = ceilf(terrain.WorldCoordsToTileCoordsY(XMVectorGetY(max)));

            //Check only the tile in its path
            for (int x = minTileX; x < maxTileX; x++)
            {
                for (int y = minTileY; y < maxTileY; y++)
                {
                    if (!(x < 0) && !(y < 0) && !(y >= terrain.tiles.size()) && !(x >= terrain.tiles.at(0).size()) && terrain.tiles.at(y).at(x) != nullptr)
                    {
                        //Collision with terrain?
                        if (Collision::GJK2DCollision(EnemyComponent::parent_.lock()->GetComponent<CollisionComponent>()->GetBoundingBox(),
                            terrain.tiles.at(y).at(x)->GetComponent<CollisionComponent>()->GetBoundingBox()))
                        {
                            //Kill the projectile
                            EnemyComponent::parent_.lock()->GetComponent<StatusComponent>()->Kill();
                        }
                    }
                }
            }


            //Move
            EnemyComponent::parent_.lock()->GetComponent<TransformComponent>()->Move(EnemyComponent::moveSpeed_.x * dt, EnemyComponent::moveSpeed_.y * dt);
        }
    }

}