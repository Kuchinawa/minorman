#pragma once

#include "EnemyComponent.h"

#include "..\Graphics\Dx11Context.h"
#include "..\Graphics\SpriteRenderer.h"
#include "..\Graphics\Sprite.h"

namespace MinorManEngine
{
    class LaserCannonComponent : public EnemyComponent
    {
    public:
        LaserCannonComponent(const std::shared_ptr<GameObject>& parent, const bool damagesOnTouch, const bool orientation);

        void UpdateAI(const float dt,
            GameObject* player,
            std::shared_ptr<Dx11Context> &dx11Context,
            const std::shared_ptr<SpriteRenderer> &spriteRenderer,
            std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
            std::vector<std::shared_ptr<GameObject>> &bulletList);

    private:

        bool shooting_;
        Sprite laser_;

    };
}