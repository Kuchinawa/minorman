#include "RenderGOComponent.h"
#include <DirectXMath.h>

#include "..\Graphics\VertexPos.h"

using namespace MinorManEngine;
using namespace DirectX;

RenderGOComponent::RenderGOComponent(const std::shared_ptr<GameObject>& parent,
    const std::shared_ptr<Dx11Context>& directXContext,
    const std::shared_ptr<SpriteRenderer>& spriteRenderer,
    const std::shared_ptr<Sprite>& defaultSprite)
    : GOComponent()//, vertexBuffer_(0), colorMapSampler_(0), mvpCB_(0), alphaBlendState_(0)
{
    parent_ = parent;
    //spriteRenderer_ = spriteRenderer;
    sprite_ = defaultSprite;
}

RenderGOComponent::~RenderGOComponent()
{
}

void RenderGOComponent::Uninitialize()
{
    sprite_->UnloadContent();

}

void RenderGOComponent::Render(const Camera &camera)
{
    //Set the model, view matrix
    XMMATRIX world = parent_.lock()->GetComponent<TransformComponent>()->GetWorldMatrix();
    XMMATRIX mv = XMMatrixMultiply(world, camera.GetCameraMatrix());

    sprite_->Render(mv);
}

void RenderGOComponent::Update(const float dt)
{
    sprite_->Update(dt);
}

void RenderGOComponent::SetHorizontalOrientation(const bool right)
{
    sprite_->SetHorizontalOrientation(right);
}

void RenderGOComponent::SetSprite(const std::shared_ptr<Sprite>& sprite)
{
    sprite_ = sprite;
}

std::shared_ptr<Sprite> RenderGOComponent::GetSprite()
{
    return sprite_;
}