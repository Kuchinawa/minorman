#pragma once

#include "GameObject.h"
#include "GOComponent.h"

#include "HeroState.h"

#include "..\Graphics\Texture.h"
#include "..\Graphics\TileRenderer.h"

#include <memory>

namespace MinorManEngine
{
    class HeroControlComponent : public GOComponent
    {
    public:
        HeroControlComponent(const std::shared_ptr<GameObject>& parent);

        void Update(const float dt);

        void HandleInput(
            const float dt,
            GameObject* hero,
            std::shared_ptr<Dx11Context> dx11Context,
            std::shared_ptr<SpriteRenderer> spriteRenderer,
            std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
            InputContext &inputContext,
            const TileRenderer &level,
            std::vector<std::shared_ptr<GameObject>> &enemies);

        void SetState(std::unique_ptr<HeroState> newState);

        bool onGround;
        bool orientation; //True = right
        bool hitCeiling;

        float blinkTransparency;

        float speedPerSec;
        float jumpSpeedY;
        float chargeSpeedX;
        //float jumpTime;

    private:
        std::weak_ptr<GameObject> parent_;

        std::unique_ptr<HeroState> currentState_;
    };
}