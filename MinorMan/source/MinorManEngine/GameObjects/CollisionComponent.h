#pragma once

#include "GameObject.h"
#include "GOComponent.h"

#include "..\Physics\AABB.h"

#include <DirectXMath.h>

#include <memory>

namespace MinorManEngine
{
    class CollisionComponent : public GOComponent
    {
    public:
        CollisionComponent(const std::shared_ptr<GameObject>& parent, std::vector<DirectX::XMFLOAT2> boundingBox, DirectX::XMFLOAT2 center, DirectX::XMFLOAT2 halfExtends);

        std::vector<DirectX::XMVECTOR> GetBoundingBox() const;
        void SetBoundingBox(std::vector<DirectX::XMFLOAT2> boundingBox);

        //Returns the bottom right corner
        DirectX::XMVECTOR GetMaxExtend();
        //Returns the top left corner
        DirectX::XMVECTOR GetMinExtend();

        AABB aABB;

    private:
        std::weak_ptr<GameObject> parent_;

        std::vector<DirectX::XMFLOAT2> boundingBox_;
        
       
    };
}