#pragma once

#include "UIElement.h"

#include "..\Graphics\Dx11Context.h"
#include "..\Graphics\Sprite.h"
#include "..\Graphics\SpriteRenderer.h"

#include <DirectXMath.h>

namespace MinorManEngine
{
    class UIHealth : public UIElement
    {
    public:
        UIHealth();
        UIHealth(const std::shared_ptr<Sprite> sprite,
            const std::shared_ptr<Dx11Context> directXContext,
            const std::shared_ptr<SpriteRenderer> spriteRenderer,
            const DirectX::XMFLOAT2 position);

        void UpdateHealth(const int health, const int maxHealth);

    private:
    };
}