#pragma once

#include "..\Graphics\Dx11Context.h"
#include "..\Graphics\Sprite.h"
#include "..\Graphics\SpriteRenderer.h"

#include <DirectXMath.h>

namespace MinorManEngine
{
    class UIElement
    {
    public:
        UIElement();
        UIElement(const std::shared_ptr<Sprite> sprite, const std::shared_ptr<Dx11Context> directXContext, const std::shared_ptr<SpriteRenderer> spriteRenderer, const DirectX::XMFLOAT2 position);

        virtual void Render();

        void SetPosition(const DirectX::XMVECTOR position);
        DirectX::XMVECTOR GetPosition();


    protected:

        DirectX::XMFLOAT2 position_;

        std::shared_ptr<Sprite> sprite_;

        std::shared_ptr<Dx11Context> directXContext_;
        std::shared_ptr<SpriteRenderer> spriteRenderer_;
    };
}