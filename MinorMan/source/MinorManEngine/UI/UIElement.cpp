#include "UIElement.h"

#include <DirectXMath.h>

using namespace MinorManEngine;
using namespace DirectX;

UIElement::UIElement()
{

}

UIElement::UIElement(const std::shared_ptr<Sprite> sprite, const std::shared_ptr<Dx11Context> directXContext, const std::shared_ptr<SpriteRenderer> spriteRenderer, DirectX::XMFLOAT2 position)
{
    position_ = position;

    sprite_ = sprite;

    std::shared_ptr<Dx11Context> directXContext_ = directXContext;
    std::shared_ptr<SpriteRenderer> spriteRenderer_ = spriteRenderer;
}

void UIElement::Render()
{
    XMMATRIX translation = XMMatrixTranslationFromVector(GetPosition());
    sprite_->Render(translation);
}

void UIElement::SetPosition(const DirectX::XMVECTOR position)
{
    XMStoreFloat2(&position_, position);
}

DirectX::XMVECTOR UIElement::GetPosition()
{
    return XMLoadFloat2(&position_);
}