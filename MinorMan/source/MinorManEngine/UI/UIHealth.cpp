#include "UIHealth.h"

using namespace MinorManEngine;
using namespace DirectX;

UIHealth::UIHealth()
{

}

UIHealth::UIHealth(const std::shared_ptr<Sprite> sprite,
    const std::shared_ptr<Dx11Context> directXContext,
    const std::shared_ptr<SpriteRenderer> spriteRenderer,
    const XMFLOAT2 position) : UIElement(sprite, directXContext, spriteRenderer, position)
{

}

inline float clamp(float value, float min, float max)
{
    return value < min ? min : (value > max ? max : value);
}

inline float normalize(const float x, const float xMin, const float xMax)
{
    float normalized = x;
    normalized -= xMin;
    normalized /= (xMax - xMin);

    return normalized;
}

void UIHealth::UpdateHealth(const int health, const int maxHealth)
{
    D3D11_TEXTURE2D_DESC textureDesc = sprite_->GetTexDesc();
    
    //Calculate the borders of the current texture frame
    float halfFrameWidth = sprite_->GetFrameWidth() / 2;
    float halfFrameHeight = (float)textureDesc.Height / 2.0f;

    float healthPercentage = (float)health / (float)maxHealth;
    float adjustedFrameHeight = (float)textureDesc.Height * healthPercentage;

    float top = halfFrameHeight - ((float)textureDesc.Height - adjustedFrameHeight);
    float vTop = 1.0f - normalize(adjustedFrameHeight, 0.0f, (float)textureDesc.Height);

    //Generate the vertices based on the image
    std::vector<VertexPos> vertices_ =
    {
        { XMFLOAT3(halfFrameWidth, top, 1.0f), XMFLOAT2(1.0f, vTop) }, //Top Right
        { XMFLOAT3(halfFrameWidth, -halfFrameHeight, 1.0f), XMFLOAT2(1.0f, 1.0f) }, //Bottom Right
        { XMFLOAT3(-halfFrameWidth, -halfFrameHeight, 1.0f), XMFLOAT2(0.0f, 1.0f) }, //Bottom Left

        { XMFLOAT3(-halfFrameWidth, -halfFrameHeight, 1.0f), XMFLOAT2(0.0f, 1.0f) }, //Bottom Left
        { XMFLOAT3(-halfFrameWidth, top, 1.0f), XMFLOAT2(0.0f, vTop) }, //Top Left
        { XMFLOAT3(halfFrameWidth, top, 1.0f), XMFLOAT2(1.0f, vTop) } //Top Right
    };

    sprite_->SetCurrentFrameCoordinates(vertices_);
}