#pragma once

namespace MinorManEngine
{
    class Config
    {
    public:

        static const int ScreenX;
        static const int ScreenY;

        static const float gravity;

        static const float heroSpeed;
        static const float heroJumpSpeedY;
        static const float heroChargeSpeed;
        static const float heroChargeDuration;
        static const float heroTempInvulDur;
        static const float heroInvulBlinkInterval;
        static const int heroHealth;
        static const int heroDamage;
        
        
        static const int walkCannonSpeed;
        static const int walkCannonHealth;
        static const int walkCannonDamage;
        static const int walkCannonProjectileDamage;

        static const int laserCannonHealth;
        static const int laserCannonDamage;

        static const int lavaHealth;
        static const int lavaDamage;

        static const int meteorHealth;
        static const int meteorDamage;

        static const float healthBarPositionX;
        static const float healthBarPositionY;

    };
}