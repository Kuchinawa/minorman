#include "MeteorEventTrigger.h"

#include "..\GameObjects\TransformComponent.h"
#include "..\GameObjects\RenderGOComponent.h"
#include "..\GameObjects\CollisionComponent.h"
#include "..\GameObjects\StatusComponent.h"
#include "..\GameObjects\ProjectileEnemyComponent.h"
#include "..\GameObjects\EnemyComponent.h"

#include "..\Graphics\SpriteFileDescriptions.h"

#include "..\Config.h"

using namespace MinorManEngine;
using namespace DirectX;

MeteorEventTrigger::MeteorEventTrigger() : EventTrigger(std::vector<XMFLOAT2>() = { { -32.0f, 32.0f }, { 32.0f, 32.0f }, { 32.0f, -32.0f }, { -32.0f, -32.0f } }, XMFLOAT2(0.0f, 0.0f))
{
    cooldown_ = 10.0f;
    triggered_ = false;
}

MeteorEventTrigger::MeteorEventTrigger(std::vector<DirectX::XMFLOAT2> boundingBox, DirectX::XMFLOAT2 position, float cooldown) : EventTrigger(boundingBox, position)
{
    cooldown_ = cooldown;
    triggered_ = false;
}

void MeteorEventTrigger::Update(const float dt,
    GameObject* player,
    std::shared_ptr<Dx11Context> &dx11Context,
    const std::shared_ptr<SpriteRenderer> &spriteRenderer,
    std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
    std::vector<std::shared_ptr<GameObject>> &meteorList)
{
    //Not triggered yet?
    if (!triggered_)
    {
        //Should we trigger?
        if (Trigger(player))
        {
            triggered_ = true;
            timePassed_ = 0.0f;
        }
    }
    else
    {
        timePassed_ += dt;

        for (int i = 0; i < spawnLocations_.size(); i++)
        {
            //If not yet spawned and its time to spawn
            if (!spawned_.at(i) && (timePassed_ >= spawnTimers_.at(i)))
            {
                //Spawn meteor! BOOOOOOM!!!!
                SpawnMeteor(spawnLocations_.at(i),
                    dx11Context, 
                    spriteRenderer, 
                    textures, 
                    meteorList);

                spawned_.at(i) = true;
            }
        }



        //Cooldown over?
        if ((timePassed_ >= cooldown_))
        {
            triggered_ = false;

            for (int i = 0; i < spawned_.size(); i++)
            {
                spawned_.at(i) = false;
            }
        }
    }
}

void MeteorEventTrigger::SpawnMeteor(DirectX::XMFLOAT2 location,
    std::shared_ptr<Dx11Context> &dx11Context,
    const std::shared_ptr<SpriteRenderer> &spriteRenderer,
    std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
    std::vector<std::shared_ptr<GameObject>> &meteorList,
    bool direction)
{
    //Create a meteor
    std::shared_ptr<GameObject> meteor = std::make_shared<GameObject>();

    //Spawn
    std::unique_ptr<GOComponent> meteorTransformComp(new TransformComponent(location.x, location.y));
    meteor->AddComponent(std::move(meteorTransformComp));

    //Add collision data
    std::unique_ptr<GOComponent> collisionComp(new CollisionComponent(
        meteor,
        std::vector<XMFLOAT2>() =
    {
        { 0, 12 }, //tl
        { 20, 21 }, //t
        { 40, 12 }, //tr
        { 48, -7 }, //r
        { 40, -26 }, //br
        { 20, -35 },//b
        { 0, -26 },//bl
        { -8, -7 } //l
    },
        XMFLOAT2(85.0f, 57.0f), XMFLOAT2(28.0f, 28.0f))
        );

    meteor->AddComponent(std::move(collisionComp));

    //Rendering
    std::shared_ptr<Sprite> meteorSprite(new Sprite(
        dx11Context,
        spriteRenderer,
        textures[SpriteFileDescriptions::Meteor.fileName],
        SpriteFileDescriptions::Meteor));
    std::unique_ptr<GOComponent> meteorRenderComp(new RenderGOComponent(meteor, dx11Context, spriteRenderer, meteorSprite));
    meteor->AddComponent(std::move(meteorRenderComp));

    //Status
    std::unique_ptr<StatusComponent> meteorStatusComp(new StatusComponent(Config::meteorHealth, Config::meteorDamage, true));
    meteor->AddComponent(std::move(meteorStatusComp));

    //AI
    std::unique_ptr<EnemyComponent> meteorEnemyComp(new ProjectileEnemyComponent(meteor, XMFLOAT2(120.0f, -120.0f), 5.0f, true));
    meteor->AddComponent(std::move(meteorEnemyComp));

    meteor->Initialize();

    meteorList.push_back(meteor);

}

void MeteorEventTrigger::AddSpawnPoint(DirectX::XMFLOAT2 location, float spawnTimer, bool direction)
{
    spawnLocations_.push_back(location);
    spawnTimers_.push_back(spawnTimer);
    spawned_.push_back(false);
}