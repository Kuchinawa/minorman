#pragma once

#include "..\GameObjects\GameObject.h"

#include <vector>
#include <DirectXMath.h>

namespace MinorManEngine
{
    class EventTrigger
    {
    public:
        EventTrigger(std::vector<DirectX::XMFLOAT2> boundingBox, DirectX::XMFLOAT2 position);

        std::vector<DirectX::XMVECTOR> EventTrigger::GetBoundingBox() const;

        bool Trigger(GameObject* triggerObject);

    private:
        std::vector<DirectX::XMFLOAT2> boundingBox_;
        DirectX::XMFLOAT2 position_;

    };
}