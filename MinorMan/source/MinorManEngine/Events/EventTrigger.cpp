#include "EventTrigger.h"

#include "..\GameObjects\CollisionComponent.h"

#include "..\Physics\Collision.h"

using namespace MinorManEngine;
using namespace DirectX;

EventTrigger::EventTrigger(std::vector<XMFLOAT2> boundingBox, XMFLOAT2 position)
{
    boundingBox_ = boundingBox;
    position_ = position;
}

bool EventTrigger::Trigger(GameObject* triggerObject)
{
    if (Collision::GJK2DCollision(triggerObject->GetComponent<CollisionComponent>()->GetBoundingBox(), GetBoundingBox()))
    {

        return true;
    }
}


std::vector<XMVECTOR> EventTrigger::GetBoundingBox() const
{
    std::vector<XMVECTOR> boundingBox;

    for (XMFLOAT2 point : boundingBox_)
    {
        XMVECTOR realPoint = XMLoadFloat2(&position_) + XMLoadFloat2(&point);
        boundingBox.push_back(realPoint);
    }

    return boundingBox;
}