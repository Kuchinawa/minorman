#pragma once

#include "EventTrigger.h"

#include "..\Graphics\Dx11Context.h"
#include "..\Graphics\SpriteRenderer.h"
#include "..\Graphics\Texture.h"

namespace MinorManEngine
{
    class MeteorEventTrigger : public EventTrigger
    {
    public:
        MeteorEventTrigger();
        MeteorEventTrigger(std::vector<DirectX::XMFLOAT2> boundingBox, DirectX::XMFLOAT2 position, float cooldown);

        void Update(const float dt,
            GameObject* player,
            std::shared_ptr<Dx11Context> &dx11Context,
            const std::shared_ptr<SpriteRenderer> &spriteRenderer,
            std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
            std::vector<std::shared_ptr<GameObject>> &meteorList);

        void SpawnMeteor(DirectX::XMFLOAT2 location,
            std::shared_ptr<Dx11Context> &dx11Context,
            const std::shared_ptr<SpriteRenderer> &spriteRenderer,
            std::unordered_map<std::string, std::shared_ptr<Texture>>& textures,
            std::vector<std::shared_ptr<GameObject>> &meteorList, bool direction = true);

        void AddSpawnPoint(DirectX::XMFLOAT2 location, float spawnTimer, bool direction = true);

    private:

        std::vector<DirectX::XMFLOAT2> spawnLocations_;
        std::vector<float> spawnTimers_;
        std::vector<bool> spawned_;

        

        bool triggered_;
        float cooldown_;
        float timePassed_;
    };
}