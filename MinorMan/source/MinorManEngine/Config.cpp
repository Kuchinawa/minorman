#include "Config.h"

using namespace MinorManEngine;

const int Config::ScreenX = 800;
const int Config::ScreenY = 600;

const float Config::gravity = -200.0f;

const float Config::heroSpeed = 200.0f;
const float Config::heroJumpSpeedY = 300.0f;
const float Config::heroChargeSpeed = 300.0f;
const float Config::heroChargeDuration = 0.35f;
const float Config::heroTempInvulDur = 2.0f;
const float Config::heroInvulBlinkInterval = 0.1f;
const int Config::heroHealth = 10;
const int Config::heroDamage = 1;

const int Config::walkCannonSpeed = -64.0f;
const int Config::walkCannonHealth = 3;
const int Config::walkCannonDamage = 1;
const int Config::walkCannonProjectileDamage = 2;

const int Config::laserCannonHealth = 3;
const int Config::laserCannonDamage = 3;

const int Config::lavaHealth = 100;
const int Config::lavaDamage = 100;

const int Config::meteorHealth = 100;
const int Config::meteorDamage= 100;

const float Config::healthBarPositionX = 40.0f;
const float Config::healthBarPositionY = 400.0f;