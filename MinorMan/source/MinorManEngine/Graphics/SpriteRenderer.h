#pragma once

#include "Dx11Context.h"

namespace MinorManEngine
{
    class SpriteRenderer
    {
    public:
        SpriteRenderer();
        ~SpriteRenderer();

        bool LoadShaders(Dx11Context& directXContext_);
        void UnloadContent();

        void Render();

        ID3D11VertexShader* GetVertexShader();
        ID3D11PixelShader* GetPixelShader();

        ID3D11InputLayout* GetInputLayout();


    private:
        //Shaders
        ID3D11VertexShader* spriteVS_;
        ID3D11PixelShader* spritePS_;

        //Shader Input Description
        ID3D11InputLayout* inputLayout_;
    };
}