#pragma once

#include <D3D11.h>
#include <D3DX11.h>
#include <DxErr.h>

namespace MinorManEngine
{
    class Dx11Context
    {
    public:
        Dx11Context();
        ~Dx11Context();

        //Initializes the DirectX Objects
        bool Initialize(HINSTANCE hInstance, HWND hwnd);

        //Releases the COM objects
        void Shutdown();

        ////Load any needed resource (Audio, textures etc...)
        //virtual bool LoadContent();
        ////Unloads the resources
        //virtual void UnloadContent();

        //Compiles a D3D Shader
        bool CompileD3DShader(const wchar_t* filePath, char* entry, char* shaderModel, ID3DBlob** buffer);

        //virtual void Update(float dt) = 0;
        //virtual void Render() = 0;

        //Clear the screen
        void ClearRenderTargetView(const float clearColor[4]);

        //Swap the render buffers
        void Swap();

        ID3D11Device* GetD3dDevice();
        ID3D11DeviceContext* GetD3dContext();

    protected:
        HINSTANCE hInstance_;
        HWND hwnd_;

        D3D_DRIVER_TYPE driverType_; //Hardware, SWAP, software...
        D3D_FEATURE_LEVEL featureLevel_; //11.0, 10.1, 10.0...

        ID3D11Device* d3dDevice_;
        ID3D11DeviceContext* d3dContext_;
        IDXGISwapChain* swapChain_;
        ID3D11RenderTargetView* backBufferTarget_;
    };
}
