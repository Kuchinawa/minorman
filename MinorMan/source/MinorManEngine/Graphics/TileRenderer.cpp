#include "TileRenderer.h"

#include "SpriteFileDescriptions.h"

#include "..\GameObjects\CollisionComponent.h"
#include "..\GameObjects\RenderGOComponent.h"
#include "..\GameObjects\TransformComponent.h"

using namespace MinorManEngine;
using namespace DirectX;

TileRenderer::TileRenderer()
{
}

void TileRenderer::LoadLevel(const std::shared_ptr<Dx11Context>& directXContext, const std::shared_ptr<SpriteRenderer>& spriteRenderer)
{
    //How many tiles fit on the screen? + 2
    numX_ = (800 / tileWidth) + 2;
    numY_ = (600 / tileHeight) + 2;

    const int dimensionsX = 30;
    const int dimensionsY = 12;

    int layout[dimensionsY][dimensionsX] =
    {
        { 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
        { 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 2 },
        { 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2 },
        { 2, 0, 0, 0, 0, 0, 0, 0, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 2 },
        { 2, 1, 2, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2 },
        { 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 1, 2, 1, 2, 1, 0, 0, 0, 2 },
        { 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2 },
        { 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 2, 1, 2 },
        { 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2 },
        { 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 1, 2, 1, 0, 0, 0, 1, 2, 1, 2, 1, 0, 1, 2, 1, 0, 0, 0, 2 },
        { 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 1, 2, 1, 0, 0, 0, 1, 2, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 2 },
        { 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2 }
    };




    tiles.resize(dimensionsY);

    for (int i = 0; i < dimensionsY; i++)
    {
        tiles.at(i).resize(dimensionsX);
    }

    for (int y = 0; y < dimensionsY; y++)
    {
        for (int x = 0; x < dimensionsX; x++)
        {
            if (layout[dimensionsY - 1 - y][x] != 0)
            {
                std::shared_ptr<GameObject> tile(new GameObject());
                std::unique_ptr<GOComponent> tileTransformComp(new TransformComponent(tileWidth / 2 + (x * tileWidth), tileHeight / 2 + (y * tileHeight)));
                tile->AddComponent(std::move(tileTransformComp));
                if (layout[dimensionsY - 1 - y][x] == 1)
                {
                    std::unique_ptr<GOComponent> tileRenderComp(new RenderGOComponent(tile, directXContext, spriteRenderer, spriteCollection_.at(0)));
                    tile->AddComponent(std::move(tileRenderComp));

                    //Add collision data
                    std::unique_ptr<GOComponent> collisionComp(new CollisionComponent(tile, std::vector<XMFLOAT2>() = { { -32.0f, 30.0f }, { 32.0f, 30.0f }, { 32.0f, -32.0f }, { -32.0f, -32.0f } }, XMFLOAT2(32.0f, 32.0f), XMFLOAT2(32.0f, 32.0f)));
                    tile->AddComponent(std::move(collisionComp));
                }
                else if (layout[dimensionsY - 1 - y][x] == 2)
                {
                    std::unique_ptr<GOComponent> tileRenderComp(new RenderGOComponent(tile, directXContext, spriteRenderer, spriteCollection_.at(1)));
                    tile->AddComponent(std::move(tileRenderComp));

                    //Add collision data
                    std::unique_ptr<GOComponent> collisionComp(new CollisionComponent(tile, std::vector<XMFLOAT2>() = { { -32.0f, 30.0f }, { 32.0f, 30.0f }, { 32.0f, -32.0f }, { -32.0f, -32.0f } }, XMFLOAT2(32.0f, 31.0f), XMFLOAT2(32.0f, 31.0f)));
                    tile->AddComponent(std::move(collisionComp));
                }

                tile->Initialize();

                //tiles.at(dimensionsY - 1 - y).at(x) = tile;
                tiles.at(y).at(x) = tile;
            }
        }
    }

    aABB_ = AABB(XMFLOAT2(dimensionsX * tileWidth / 2, dimensionsY * tileHeight / 2), XMFLOAT2(dimensionsX * tileWidth / 2, dimensionsY * tileHeight / 2));
}

void TileRenderer::LoadSpriteCollection(const std::shared_ptr<Dx11Context>& directXContext, const std::shared_ptr<SpriteRenderer>& spriteRenderer, std::vector<std::shared_ptr<Texture>>& tileTextures, const std::vector<SpriteFileDescription>& spriteDescriptions)
{
    //Load the provided Tile Sprites
    for (int i = 0; i < tileTextures.size(); i++)
    {
        Sprite TileSprite(directXContext, spriteRenderer, tileTextures.at(i), spriteDescriptions.at(i));
        TileSprite.SetCurrentFrameCoordinates();
        spriteCollection_.push_back(std::make_shared<Sprite>(TileSprite));
    }
}

void TileRenderer::Render(const Camera &camera)
{

    XMVECTOR topRight = camera.GetAABB().GetTopRight();
    XMVECTOR bottomLeft = camera.GetAABB().GetBottomLeft();

    //Get the tile coords according to the camera AABB
    int tileMaxX = ceilf(XMVectorGetX(topRight) / tileWidth);
    int tileMaxY = ceilf(XMVectorGetY(topRight) / tileHeight);

    int tileMinX = floorf(XMVectorGetX(bottomLeft) / tileWidth);
    int tileMinY = floorf(XMVectorGetY(bottomLeft) / tileHeight);

    for (int y = tileMinY; y < tileMaxY; y++)
    {
        for (int x = tileMinX; x < tileMaxX; x++)
        {
            if (y >= 0 && y < tiles.size())
            {
                if (x >= 0 && x < tiles.at(y).size())
                {
                    if (tiles.at(y).at(x) != nullptr)
                    {
                        tiles.at(y).at(x)->GetComponent<RenderGOComponent>()->Render(camera);
                    }
                }
            }
        }
    }
}

float TileRenderer::WorldCoordsToTileCoordsX(float x) const
{
    return x / tileWidth;
}

float TileRenderer::WorldCoordsToTileCoordsY(float y) const
{
    return y / tileHeight;
}

AABB TileRenderer::GetAABB() const
{
    return aABB_;
}