#pragma once

#include "Dx11Context.h"
#include "VertexPos.h"

#include "SpriteRenderer.h"
#include "SpriteFileDescription.h"
#include "Texture.h"

#include <memory>
#include <string>
#include <vector>



#include <DirectXMath.h>

namespace MinorManEngine
{
    struct VS_CONSTANT_BUFFER_SPRITE
    {
        DirectX::XMMATRIX mvpMatrix;
        float transparency;
    };

    class Sprite
    {
    public:
        Sprite(const std::shared_ptr<Dx11Context>& directXContext,
            const std::shared_ptr<SpriteRenderer>& spriteRenderer,
            std::shared_ptr<Texture>& texture,
            SpriteFileDescription spriteFileDescription,
            const bool horizontalOrientation = true , 
            const bool repeat = true, 
            const bool startAtRepeat = false, 
            const float transparency = 1.0f,
            const bool customFrameCoords = false);

        Sprite() : frameWidth_(64), currentFrame_(1), repeatFrameNr_(1), elapsedTimeSec_(0.0f), frameTimeSec_(0.0f), horizontalOrientation_(true), repeat_(true), transparency_(1.0f), directXContext_(nullptr) {};

        ~Sprite();

        void UnloadContent();

        void Update(const float dt);

        void Render(const DirectX::XMMATRIX &modelViewMatrix);

        //Sets the XY and UV coordinates
        void SetCurrentFrameCoordinates();

        //Sets the XY and UV coordinates
        void SetCurrentFrameCoordinates(std::vector<VertexPos> vertices);

        bool AnimationFinished();
        
        void SetHorizontalOrientation(const bool right);

        void SetTransparency(float transparency);

        float GetTransparency();

        float GetFrameWidth();

        D3D11_TEXTURE2D_DESC GetTexDesc();

    private:
        int frameWidth_;
        int currentFrame_;
        int repeatFrameNr_;
        int repeatFrameNrTo_;
        
        bool repeat_;
        bool customFrameCoords_;

        float elapsedTimeSec_;
        float frameTimeSec_;

        bool horizontalOrientation_; //false == left / true == right

        float transparency_;

        std::shared_ptr<Dx11Context> directXContext_;
        std::shared_ptr<SpriteRenderer> spriteRenderer_;
        std::shared_ptr<Texture> texture_;
        
        std::vector<VertexPos> vertices_;
        ID3D11BlendState* alphaBlendState_;
        
        ID3D11Buffer* vertexBuffer_;
        ID3D11Buffer* mvpCB_;
        DirectX::XMFLOAT4X4 projectionMatrix_;

        //Normalizes x to a number between 0.0 and 1.0
        static float Normalize(const float x, const float xMin, const float xMax);
    };
}