#include "SpriteRenderer.h"
#include <DirectXMath.h>

using namespace MinorManEngine;
using namespace DirectX;


SpriteRenderer::SpriteRenderer() : inputLayout_(0), spriteVS_(0), spritePS_(0)
{
}


SpriteRenderer::~SpriteRenderer()
{
    UnloadContent();
}

bool SpriteRenderer::LoadShaders(Dx11Context& directXContext)
{
    //Open the shader file and try to compile the vertex shader within it
    ID3DBlob* vsBuffer = 0;
    bool compileResult = directXContext.CompileD3DShader(L"Sprites.fx", "VS_Main", "vs_4_0", &vsBuffer);

    if (compileResult == false)
    {
        DXTRACE_MSG(L"Error compiling the vertex shader!");
        return false;
    }

    HRESULT d3dResult;

    d3dResult = directXContext.GetD3dDevice()->CreateVertexShader(vsBuffer->GetBufferPointer(), vsBuffer->GetBufferSize(), 0, &spriteVS_);

    if (FAILED(d3dResult))
    {
        DXTRACE_MSG(L"Error creating the vertex shader!");

        if (vsBuffer)
            vsBuffer->Release();

        return false;
    }

    //Describe the input variables of the vertex shader
    D3D11_INPUT_ELEMENT_DESC spritesLayout[] =
    {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
    };

    unsigned int totalLayoutElements = ARRAYSIZE(spritesLayout);

    d3dResult = directXContext.GetD3dDevice()->CreateInputLayout(spritesLayout, totalLayoutElements, vsBuffer->GetBufferPointer(), vsBuffer->GetBufferSize(), &inputLayout_);

    vsBuffer->Release();

    if (FAILED(d3dResult))
    {
        DXTRACE_MSG(L"Error creating the input layout!");
        return false;
    }

    //Open the shader file and try to compile the pixel shader within it
    ID3DBlob* psBuffer = 0;
    compileResult = directXContext.CompileD3DShader(L"Sprites.fx", "PS_Main", "ps_4_0", &psBuffer);

    if (compileResult == false)
    {
        DXTRACE_MSG(L"Error compiling pixel shader!");
        return false;
    }

    d3dResult = directXContext.GetD3dDevice()->CreatePixelShader(psBuffer->GetBufferPointer(), psBuffer->GetBufferSize(), 0, &spritePS_);

    psBuffer->Release();

    if (FAILED(d3dResult))
    {
        DXTRACE_MSG(L"Error creating pixel shader!");
        return false;
    }
}

void SpriteRenderer::UnloadContent()
{
    if (spriteVS_)
        spriteVS_->Release();
    if (spritePS_)
        spritePS_->Release();
    if (inputLayout_)
        inputLayout_->Release();

    spriteVS_ = 0;
    spritePS_ = 0;
    inputLayout_ = 0;
}

void SpriteRenderer::Render()
{
    

}

ID3D11VertexShader* SpriteRenderer::GetVertexShader()
{
    return spriteVS_;
}

ID3D11PixelShader* SpriteRenderer::GetPixelShader()
{
    return spritePS_;
}

ID3D11InputLayout* SpriteRenderer::GetInputLayout()
{
    return inputLayout_;
}