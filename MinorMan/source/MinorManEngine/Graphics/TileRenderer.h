#pragma once

#include <vector>
#include <unordered_map>

#include "..\GameObjects\GameObject.h"


#include "..\Physics\AABB.h"

#include "Sprite.h"
#include "SpriteRenderer.h"

#include "Camera.h"

namespace MinorManEngine
{
    class TileRenderer
    {
    public:
        TileRenderer();

        static const int tileWidth = 64;
        static const int tileHeight = 64;

        void LoadSpriteCollection(const std::shared_ptr<Dx11Context>& directXContext, const std::shared_ptr<SpriteRenderer>& spriteRenderer, std::vector<std::shared_ptr<Texture>>& tileTextures, const std::vector<SpriteFileDescription>& spriteDescriptions);
        void LoadLevel(const std::shared_ptr<Dx11Context>& directXContext, const std::shared_ptr<SpriteRenderer>& spriteRenderer);

        void Render(const Camera &camera);

        std::vector<std::vector<std::shared_ptr<GameObject>>> tiles;

        float WorldCoordsToTileCoordsX(float x) const;
        float WorldCoordsToTileCoordsY(float y) const;

        AABB GetAABB() const;

    private:

        std::vector<std::shared_ptr<Sprite>> spriteCollection_;

        int numX_;
        int numY_;
        int depth_;

        AABB aABB_;
    };
}