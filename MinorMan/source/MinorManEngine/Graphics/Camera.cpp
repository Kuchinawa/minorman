#include "Camera.h"

using namespace MinorManEngine;
using namespace DirectX;

Camera::Camera()
{
    position_ = XMFLOAT2(0.0f, 0.0f);
    aABB_ = AABB(XMFLOAT2(400.0f, 300.0f), XMFLOAT2(400.0f, 300.0f));
}

void Camera::Update(const float dt, const XMVECTOR followPosition, const AABB& worldAABB)
{
    XMVECTOR follow = followPosition;

    //Move 0,0 to the middle
    follow -= worldAABB.GetHalfExtends();
    
    XMVECTOR screenHalfExtents = aABB_.GetHalfExtends();
    XMVECTOR mapExtents = worldAABB.GetHalfExtends() * 2;

    //Keep the camera within the level bounds

    XMVECTOR bottomLeft = follow - screenHalfExtents;
    XMVECTOR topRight = follow + screenHalfExtents;

    XMVECTOR correctBottomLeft = XMVectorMin(bottomLeft + worldAABB.GetHalfExtends(), XMVectorZero());
    XMVECTOR correctTopRight = XMVectorMin(worldAABB.GetHalfExtends() - topRight, XMVectorZero());
    
    XMVECTOR correction = correctBottomLeft - correctTopRight;

    XMVECTOR newPosition = XMVectorNegate(followPosition) + correction;
    
    aABB_.SetCenter(XMVectorNegate(newPosition));

    //Correct for camera origin (bottom left)
    newPosition += screenHalfExtents;

    XMStoreFloat2(&position_, newPosition);
}


void Camera::SetPosition(float x, float y)
{
    position_ = XMFLOAT2(x, y);
}

void Camera::SetPosition(XMFLOAT2 newPosition)
{
    position_ = newPosition;
}

void Camera::Move(float x, float y)
{
    position_.x += x;
    position_.y += y;
}

void Camera::Move(XMFLOAT2 offset)
{
    position_.x += offset.x;
    position_.y += offset.y;
}

XMMATRIX Camera::GetCameraMatrix() const
{
    XMMATRIX translation = XMMatrixTranslation(position_.x, position_.y, 0);
    return translation;
}

AABB Camera::GetAABB() const
{
    return aABB_;
}