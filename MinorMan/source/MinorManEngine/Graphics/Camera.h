#pragma once

#include <DirectXMath.h>

#include "..\Physics\AABB.h"

namespace MinorManEngine
{
    class Camera
    {
    public:
        Camera();

        void Update(const float dt, const DirectX::XMVECTOR followPosition, const AABB& worldAABB);

        void SetPosition(float x, float y);
        void SetPosition(DirectX::XMFLOAT2 newPosition);

        void Move(float x, float y);
        void Move(DirectX::XMFLOAT2 offset);

        DirectX::XMMATRIX GetCameraMatrix() const;
        AABB GetAABB() const;

    private:
        
        DirectX::XMFLOAT2 position_;

        AABB aABB_;
    };
}