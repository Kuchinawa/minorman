#include "Texture.h"

using namespace MinorManEngine;

Texture::Texture(const std::string& fileName, const std::shared_ptr<Dx11Context>& directXContext)
{

    directXContext_ = directXContext;

    HRESULT d3dResult;

    //Load the sprite texture file
    d3dResult = D3DX11CreateShaderResourceViewFromFileA(directXContext_->GetD3dDevice(), fileName.c_str(), 0, 0, &colorMap_, 0);

    if (FAILED(d3dResult))
    {
        DXTRACE_MSG(L"Failed to load the texture image!");
    }

    //Set the sampler state
    D3D11_SAMPLER_DESC colorMapDesc;
    ZeroMemory(&colorMapDesc, sizeof(colorMapDesc));
    colorMapDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
    colorMapDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    colorMapDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    colorMapDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    colorMapDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    colorMapDesc.MaxLOD = D3D11_FLOAT32_MAX;

    d3dResult = directXContext_->GetD3dDevice()->CreateSamplerState(&colorMapDesc, &colorMapSampler_);

    if (FAILED(d3dResult))
    {
        DXTRACE_MSG(L"Failed to create color map sampler state!");
    }
}

Texture::~Texture()
{
    //if (colorMap_)
    //    colorMap_->Release();
    //if (colorMapSampler_)
    //    colorMapSampler_->Release();

    //colorMap_ = 0;
    //colorMapSampler_ = 0;
}