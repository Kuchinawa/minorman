#pragma once

#include <string>

namespace MinorManEngine
{
    class SpriteFileDescription
    {
    public:

        SpriteFileDescription(const std::string& fileName, const int frames, const int spriteWidth, const int repeatFrameNr, const int repeatFrameNrTo, const float frameTimeMS) :
            fileName(fileName),
            frames(frames),
            spriteWidth(spriteWidth),
            repeatFrameNr(repeatFrameNr),
            repeatFrameNrTo(repeatFrameNrTo),
            frameTimeMS(frameTimeMS)
        {}
        

        const std::string fileName;
        const int frames;
        const int spriteWidth;
        const int repeatFrameNr;
        const int repeatFrameNrTo;
        const float frameTimeMS;
    };
}