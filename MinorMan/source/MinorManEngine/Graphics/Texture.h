#pragma once

#include <D3D11.h>
#include <D3DX11.h>
#include <DxErr.h>

#include <string>
#include <memory>

#include "Dx11Context.h"

namespace MinorManEngine
{
    class Texture
    {
    public:

        //Holds a texture file in a Shader Resource View and its Sampler State
        Texture(const std::string& fileName, const std::shared_ptr<Dx11Context>& directXContext);

        ~Texture();

        //A pointer to a shader resource view that contains the texture
        ID3D11ShaderResourceView* colorMap_;
        //A pointer to the textures sampler state
        ID3D11SamplerState* colorMapSampler_;

    private:
        std::shared_ptr<Dx11Context> directXContext_;
        
    };
}