#pragma once

#include "SpriteFileDescription.h"

namespace MinorManEngine
{
    static class SpriteFileDescriptions
    {
    public:
        //Player
        static const SpriteFileDescription HeroRunning;
        static const SpriteFileDescription HeroIdle;
        static const SpriteFileDescription HeroCharge;
        static const SpriteFileDescription HeroJump;
        static const SpriteFileDescription HeroAttack;

        //Enemies
        static const SpriteFileDescription WalkCannonWalking;
        static const SpriteFileDescription WalkCannonAttacking;
        static const SpriteFileDescription WalkCannonBullet;

        static const SpriteFileDescription laserCannon;
        static const SpriteFileDescription laserCannonShoot;

        static const SpriteFileDescription laser;
        
        //Background
        static const SpriteFileDescription Background;

        //Terrain
        static const SpriteFileDescription GroundM;
        static const SpriteFileDescription GroundM2;
        static const SpriteFileDescription Lava;
        static const SpriteFileDescription Meteor;

        //UI
        static const SpriteFileDescription HealthContainer;
        static const SpriteFileDescription HealthBar;
        static const SpriteFileDescription HealthBarOne;
    };
}