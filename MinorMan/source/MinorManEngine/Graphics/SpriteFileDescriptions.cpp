#include "SpriteFileDescriptions.h"

using namespace MinorManEngine;

//Player
const SpriteFileDescription SpriteFileDescriptions::HeroRunning = SpriteFileDescription("running.png", 16, 64, 3, 15, 0.07f);
const SpriteFileDescription SpriteFileDescriptions::HeroIdle = SpriteFileDescription("idle.png", 4, 64, 1, 4, 0.2f);
const SpriteFileDescription SpriteFileDescriptions::HeroCharge = SpriteFileDescription("charge.png", 3, 135, 2, 3, 0.1f);
const SpriteFileDescription SpriteFileDescriptions::HeroJump = SpriteFileDescription("jump.png", 11, 64, 9, 10, 0.1f);
const SpriteFileDescription SpriteFileDescriptions::HeroAttack = SpriteFileDescription("attack.png", 17, 144, 1, 17, 0.029f);

//Enemies
const SpriteFileDescription SpriteFileDescriptions::WalkCannonWalking = SpriteFileDescription("walkCannonWalk.png", 6, 64, 1, 6, 0.1f);
const SpriteFileDescription SpriteFileDescriptions::WalkCannonAttacking = SpriteFileDescription("walkCannonAttack.png", 3, 64, 1, 3, 0.1f);
const SpriteFileDescription SpriteFileDescriptions::WalkCannonBullet = SpriteFileDescription("walkerBullet.png", 2, 16, 1, 2, 0.5f);

const SpriteFileDescription SpriteFileDescriptions::laserCannon = SpriteFileDescription("lasercannon.png", 17, 128, 1, 17, 0.25f);;
const SpriteFileDescription SpriteFileDescriptions::laserCannonShoot = SpriteFileDescription("lasercannonshoot.png", 8, 72, 1, 8, 0.25f);;
const SpriteFileDescription SpriteFileDescriptions::laser = SpriteFileDescription("laser.png", 8, 32, 1, 8, 0.25f);;

//Background
const SpriteFileDescription SpriteFileDescriptions::Background = SpriteFileDescription("background.png", 1, 64, 1, 1, 1);

//Terrain
const SpriteFileDescription SpriteFileDescriptions::GroundM = SpriteFileDescription("groundM.png", 1, 64, 1, 1, 1);
const SpriteFileDescription SpriteFileDescriptions::GroundM2 = SpriteFileDescription("groundM2.png", 1, 64, 1, 1, 1);

const SpriteFileDescription SpriteFileDescriptions::Lava = SpriteFileDescription("lava.png", 3, 64, 1, 3, 0.5f);
const SpriteFileDescription SpriteFileDescriptions::Meteor = SpriteFileDescription("meteor.png", 13, 128, 1, 13, 0.1f);

//UI
const SpriteFileDescription SpriteFileDescriptions::HealthContainer = SpriteFileDescription("healthbar.png", 1, 32, 1, 1, 1000.0f);
const SpriteFileDescription SpriteFileDescriptions::HealthBar = SpriteFileDescription("health.png", 1, 12, 1, 1, 1000.0f);
const SpriteFileDescription SpriteFileDescriptions::HealthBarOne = SpriteFileDescription("onehealth.png", 1, 12, 1, 1, 1000.0f);