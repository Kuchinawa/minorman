#include "Sprite.h"

#include <DirectXMath.h>

#include "..\Config.h"

using namespace MinorManEngine;
using namespace DirectX;

Sprite::Sprite(const std::shared_ptr<Dx11Context>& directXContext,
    const std::shared_ptr<SpriteRenderer>& spriteRenderer,
    std::shared_ptr<Texture>& texture,
    SpriteFileDescription spriteFileDescription,
    const bool horizontalOrientation,
    const bool repeat,
    const bool startAtRepeat,
    const float transparency,
    const bool customFrameCoords)
{
    directXContext_ = directXContext;
    spriteRenderer_ = spriteRenderer;
    texture_ = texture;
    frameWidth_ = spriteFileDescription.spriteWidth;
    frameTimeSec_ = spriteFileDescription.frameTimeMS;
    repeatFrameNr_ = spriteFileDescription.repeatFrameNr;
    repeatFrameNrTo_ = spriteFileDescription.repeatFrameNrTo;

    repeat_ = repeat;
    customFrameCoords_ = customFrameCoords;

    horizontalOrientation_ = horizontalOrientation;

    transparency_ = transparency;

    if (!startAtRepeat)
    {
        currentFrame_ = 1;
    }
    else
    {
        currentFrame_ = spriteFileDescription.repeatFrameNr;
    }

    elapsedTimeSec_ = 0.0f;

    SetCurrentFrameCoordinates();


    //Init

    HRESULT d3dResult;

    D3D11_BUFFER_DESC vertexDesc;
    ZeroMemory(&vertexDesc, sizeof(vertexDesc));
    vertexDesc.Usage = D3D11_USAGE_DYNAMIC;
    vertexDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    vertexDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vertexDesc.ByteWidth = sizeof(VertexPos)* 6;

    D3D11_SUBRESOURCE_DATA resourceData;
    ZeroMemory(&resourceData, sizeof(resourceData));
    resourceData.pSysMem = &((vertices_)[0]); //Get address to the data within the vector

    d3dResult = directXContext_->GetD3dDevice()->CreateBuffer(&vertexDesc, &resourceData, &vertexBuffer_);

    if (FAILED(d3dResult))
    {
        DXTRACE_MSG(L"Failed to create vertex buffer!");
    }

    D3D11_BUFFER_DESC constDesc;
    ZeroMemory(&constDesc, sizeof(constDesc));
    constDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    constDesc.ByteWidth = sizeof(VS_CONSTANT_BUFFER_SPRITE);
    constDesc.Usage = D3D11_USAGE_DEFAULT;

    d3dResult = directXContext_->GetD3dDevice()->CreateBuffer(&constDesc, 0, &mvpCB_);

    if (FAILED(d3dResult))
    {
        DXTRACE_MSG(L"Failed to create matrix buffer!");
    }

    //Sets 0,0 bottom left
    XMMATRIX projection = XMMatrixOrthographicOffCenterLH(0.0f, Config::ScreenX, 0.0f, Config::ScreenY, 0.1f, 100.0f);
    XMStoreFloat4x4(&projectionMatrix_, projection);

    D3D11_BLEND_DESC blendDesc;
    ZeroMemory(&blendDesc, sizeof(blendDesc));
    blendDesc.RenderTarget[0].BlendEnable = TRUE;
    blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
    blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
    blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_INV_DEST_ALPHA;
    blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[0].RenderTargetWriteMask = 0x0F;

    float blendFactor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

    directXContext_->GetD3dDevice()->CreateBlendState(&blendDesc, &alphaBlendState_);
    directXContext_->GetD3dContext()->OMSetBlendState(alphaBlendState_, blendFactor, 0xFFFFFFFF);
}

void Sprite::SetCurrentFrameCoordinates()
{
    //Get the texture description
    ID3D11Resource* colorTex;
    texture_->colorMap_->GetResource(&colorTex);

    D3D11_TEXTURE2D_DESC colorTexDesc;
    ((ID3D11Texture2D*)colorTex)->GetDesc(&colorTexDesc);
    colorTex->Release();

    //Get borders of the current texture frame
    float frameWidth = frameWidth_;
    float frameLeft = Normalize(((currentFrame_ * frameWidth) - frameWidth), 0.0f, (float)colorTexDesc.Width);
    float frameRight = Normalize((currentFrame_ * frameWidth), 0.0f, (float)colorTexDesc.Width);
    float halfFrameHeight = (float)colorTexDesc.Height / 2.0f;
    float halfFrameWidth = frameWidth / 2;

    if (!horizontalOrientation_)
    {
        std::swap(frameLeft, frameRight);
    }

    //Generate the vertices based on the image size
    vertices_ =
    {
        { XMFLOAT3(halfFrameWidth, halfFrameHeight, 1.0f), XMFLOAT2(frameRight, 0.0f) }, //Top Right
        { XMFLOAT3(halfFrameWidth, -halfFrameHeight, 1.0f), XMFLOAT2(frameRight, 1.0f) }, //Bottom Right
        { XMFLOAT3(-halfFrameWidth, -halfFrameHeight, 1.0f), XMFLOAT2(frameLeft, 1.0f) }, //Bottom Left

        { XMFLOAT3(-halfFrameWidth, -halfFrameHeight, 1.0f), XMFLOAT2(frameLeft, 1.0f) }, //Bottom Left
        { XMFLOAT3(-halfFrameWidth, halfFrameHeight, 1.0f), XMFLOAT2(frameLeft, 0.0f) }, //Top Left
        { XMFLOAT3(halfFrameWidth, halfFrameHeight, 1.0f), XMFLOAT2(frameRight, 0.0f) } //Top Right
    };
}

void Sprite::SetCurrentFrameCoordinates(std::vector<VertexPos> vertices)
{
    vertices_ = vertices;
}

Sprite::~Sprite()
{
    //UnloadContent();
}

void Sprite::UnloadContent()
{
    if (vertexBuffer_)
        vertexBuffer_->Release();
    if (mvpCB_)
        mvpCB_->Release();
    if (alphaBlendState_)
        alphaBlendState_->Release();

    vertexBuffer_ = 0;
    mvpCB_ = 0;
    alphaBlendState_ = 0;
}

float Sprite::Normalize(const float x, const float xMin, const float xMax)
{
    float normalized = x;
    normalized -= xMin;
    normalized /= (xMax - xMin);

    return normalized;
}

void Sprite::Update(float dt)
{
    if ((repeat_ || currentFrame_ != repeatFrameNrTo_))
    {
        elapsedTimeSec_ += dt;
    }

    //Switch the current frame, stop if repeat is false and we reached the last frame
    while (elapsedTimeSec_ >= frameTimeSec_ && (repeat_ || currentFrame_ != repeatFrameNrTo_))
    {
        if (currentFrame_ == repeatFrameNrTo_)
            currentFrame_ = repeatFrameNr_;
        else
            ++currentFrame_;

        elapsedTimeSec_ -= frameTimeSec_;
    }

    if (!customFrameCoords_)
    {
        SetCurrentFrameCoordinates();
    }
}

void Sprite::SetHorizontalOrientation(const bool right)
{
    horizontalOrientation_ = right;
}

bool Sprite::AnimationFinished()
{
    return currentFrame_ == repeatFrameNrTo_;
}

void Sprite::SetTransparency(float transparency)
{
    transparency_ = transparency;
}

float Sprite::GetTransparency()
{
    return transparency_;
}

float Sprite::GetFrameWidth()
{
    return frameWidth_;
}

void Sprite::Render(const DirectX::XMMATRIX &modelViewMatrix)
{
    unsigned int stride = sizeof(VertexPos);
    unsigned int offset = 0;

    //Setup the vertex buffer
    directXContext_->GetD3dContext()->IASetInputLayout(spriteRenderer_->GetInputLayout());
    directXContext_->GetD3dContext()->IASetVertexBuffers(0, 1, &vertexBuffer_, &stride, &offset);
    directXContext_->GetD3dContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    //Load the shaders
    directXContext_->GetD3dContext()->VSSetShader(spriteRenderer_->GetVertexShader(), 0, 0);
    directXContext_->GetD3dContext()->PSSetShader(spriteRenderer_->GetPixelShader(), 0, 0);
    directXContext_->GetD3dContext()->PSSetShaderResources(0, 1, &texture_->colorMap_);
    directXContext_->GetD3dContext()->PSSetSamplers(0, 1, &texture_->colorMapSampler_);

    ////Set Matrixes and send to the buffer
    XMMATRIX mvp = XMMatrixMultiply(modelViewMatrix, XMLoadFloat4x4(&projectionMatrix_));
    mvp = XMMatrixTranspose(mvp);


    VS_CONSTANT_BUFFER_SPRITE vsCBSprite;

    vsCBSprite.mvpMatrix = mvp;
    vsCBSprite.transparency = transparency_;

    directXContext_->GetD3dContext()->UpdateSubresource(mvpCB_, 0, 0, &vsCBSprite, 0, 0);
    directXContext_->GetD3dContext()->VSSetConstantBuffers(0, 1, &mvpCB_);
    directXContext_->GetD3dContext()->PSSetConstantBuffers(0, 1, &mvpCB_);

    D3D11_MAPPED_SUBRESOURCE mapResource;
    HRESULT d3dResult = directXContext_->GetD3dContext()->Map(vertexBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapResource);

    if (FAILED(d3dResult))
    {
        DXTRACE_MSG(L"Failed to load map resource!");
        return;
    }

    VertexPos *bufferDataPtr = (VertexPos*)mapResource.pData;

    bufferDataPtr[0] = vertices_[0];
    bufferDataPtr[1] = vertices_[1];
    bufferDataPtr[2] = vertices_[2];
    bufferDataPtr[3] = vertices_[3];
    bufferDataPtr[4] = vertices_[4];
    bufferDataPtr[5] = vertices_[5];

    directXContext_->GetD3dContext()->Unmap(vertexBuffer_, 0);

    directXContext_->GetD3dContext()->Draw(6, 0);
}

D3D11_TEXTURE2D_DESC Sprite::GetTexDesc()
{
    //Get the texture description
    ID3D11Resource* colorTex;
    texture_->colorMap_->GetResource(&colorTex);

    D3D11_TEXTURE2D_DESC texDesc;
    ((ID3D11Texture2D*)colorTex)->GetDesc(&texDesc);
    colorTex->Release();

    return texDesc;
}