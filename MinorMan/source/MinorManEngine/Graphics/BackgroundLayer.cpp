#include "BackgroundLayer.h"

#include <DirectXMath.h>

using namespace MinorManEngine;
using namespace DirectX;

BackgroundLayer::BackgroundLayer()
{
}

BackgroundLayer::BackgroundLayer(const std::shared_ptr<Dx11Context>& directXContext,
    const std::shared_ptr<SpriteRenderer>& spriteRenderer,
    const std::shared_ptr<Sprite>& backgroundSprite)
{
    directXContext_ = directXContext;
    spriteRenderer_ = spriteRenderer;
    sprite_ = backgroundSprite;
}

void BackgroundLayer::Update(const float dt)
{

}

void BackgroundLayer::Render(const Camera& camera)
{
    D3D11_TEXTURE2D_DESC colorTexDesc = sprite_->GetTexDesc();

    float texWidth = 832.0f;
    float texHeight = 640.0f;

    float halfFrameWidth = texWidth / 2;
    float halfFrameHeight = texHeight / 2;

    //Get max edge of the camera bounds
    XMVECTOR edge = camera.GetAABB().GetTopRight();
    float right = XMVectorGetX(edge);
    float top = XMVectorGetY(edge);

    //Divide the edge with the size of the texture so we get a scrolling effect
    right /= texWidth;
    top /= texHeight;

    //Generate the vertices based on the image size (V is reversed so top == bottom)
    std::vector<VertexPos> vertices_ =
    {
        { XMFLOAT3(halfFrameWidth, halfFrameHeight, 1.0f), XMFLOAT2(right, top - 2.0f) }, //Top Right
        { XMFLOAT3(halfFrameWidth, -halfFrameHeight, 1.0f), XMFLOAT2(right, top) }, //Bottom Right
        { XMFLOAT3(-halfFrameWidth, -halfFrameHeight, 1.0f), XMFLOAT2(right - 2.0f, top) }, //Bottom Left

        { XMFLOAT3(-halfFrameWidth, -halfFrameHeight, 1.0f), XMFLOAT2(right - 2.0f, top) }, //Bottom Left
        { XMFLOAT3(-halfFrameWidth, halfFrameHeight, 1.0f), XMFLOAT2(right - 2.0f, top - 2.0f) }, //Top Left
        { XMFLOAT3(halfFrameWidth, halfFrameHeight, 1.0f), XMFLOAT2(right, top - 2.0f) } //Top Right
    };

    sprite_->SetCurrentFrameCoordinates(vertices_);

    XMMATRIX zero = XMMatrixIdentity();
    XMMATRIX translation = XMMatrixTranslation(400.0f, 300.0f, 0.0f);

    sprite_->Render(translation);
}