#pragma once

#include <DirectXMath.h>


namespace MinorManEngine
{
    struct VertexPos
    {
        DirectX::XMFLOAT3 pos;
        DirectX::XMFLOAT2 tex0;
    };
}