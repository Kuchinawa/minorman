#pragma once

#include "Camera.h"
#include "Sprite.h"
#include "Texture.h"


namespace MinorManEngine
{
    class BackgroundLayer
    {
    public:

        BackgroundLayer();

        BackgroundLayer(const std::shared_ptr<Dx11Context>& directXContext,
            const std::shared_ptr<SpriteRenderer>& spriteRenderer,
            const std::shared_ptr<Sprite>& backgroundSprite);

        void Update(const float dt);

        void Render(const Camera& camera);

    private:

        std::shared_ptr<Sprite> sprite_;

        std::shared_ptr<Dx11Context> directXContext_;
        std::shared_ptr<SpriteRenderer> spriteRenderer_;
    };
}