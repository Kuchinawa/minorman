#include "InputContext.h"

using namespace MinorManEngine;

InputContext::InputContext()
{

}

InputContext::~InputContext()
{

}

bool InputContext::Initialize(HINSTANCE hInstance, HWND hwnd)
{
    hInstance_ = hInstance;
    hwnd_ = hwnd;

    HRESULT result;

    //Input
    result = DirectInput8Create(hInstance_, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&directInput_, 0);

    if (FAILED(result))
    {
        return false;
    }

    //Keyboard
    ZeroMemory(keyboardKeys_, sizeof(keyboardKeys_));
    ZeroMemory(prevKeyboardKeys_, sizeof(prevKeyboardKeys_));

    result = directInput_->CreateDevice(GUID_SysKeyboard, &keyboardDevice_, 0);

    if (FAILED(result))
    {
        return false;
    }

    result = keyboardDevice_->SetDataFormat(&c_dfDIKeyboard);

    if (FAILED(result))
    {
        return false;
    }

    result = keyboardDevice_->SetCooperativeLevel(hwnd_, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);

    if (FAILED(result))
    {
        return false;
    }

    result = keyboardDevice_->Acquire();

    //if (FAILED(result))
    //{
    //    return false;
    //}

    return true;
}

void InputContext::Shutdown()
{
    if (keyboardDevice_)
    {
        keyboardDevice_->Unacquire();
        keyboardDevice_->Release();
    }

    if (directInput_)
    {
        directInput_->Release();
    }

    keyboardDevice_ = 0;
    directInput_ = 0;
}

void InputContext::Update()
{
    HRESULT result;
    //Store prev keyboard state
    memcpy(prevKeyboardKeys_, keyboardKeys_, sizeof(keyboardKeys_));
    //Get new
    result = keyboardDevice_->GetDeviceState(sizeof(keyboardKeys_), (LPVOID)&keyboardKeys_);

    if (FAILED(result))
    {
        if ((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
        {
            result = keyboardDevice_->Acquire();
        }
    }
}

bool InputContext::KeyUp(const int key)
{
    if (KEYDOWN(prevKeyboardKeys_, key) && !KEYDOWN(keyboardKeys_, key))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool InputContext::KeyDown(const int key)
{
    if (KEYDOWN(keyboardKeys_, key))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool InputContext::KeyPressed(const int key)
{
    if (!KEYDOWN(prevKeyboardKeys_, key) && KEYDOWN(keyboardKeys_, key))
    {
        return true;
    }
    else
    {
        return false;
    }
}