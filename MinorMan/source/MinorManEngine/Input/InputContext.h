#pragma once

#include <dinput.h>

#define KEYDOWN(name, key) (name[key] & 0x80)
#define BUTTONDOWN(name, key) (name.rgbButtons[key] & 0x80)

namespace MinorManEngine
{
    class InputContext
    {
    public:
        InputContext();
        ~InputContext();

        bool Initialize(HINSTANCE hInstance, HWND hwnd);
        void Shutdown();

        void Update();

        //Checks if a given key is released
        bool KeyUp(const int key);

        //Checks if a given key is currently being pressed
        bool KeyDown(const int key);

        //Check if a given key was pressed
        bool KeyPressed(const int key);

    private:
        HINSTANCE hInstance_;
        HWND hwnd_;

        LPDIRECTINPUT8 directInput_;
        LPDIRECTINPUTDEVICE8 keyboardDevice_;
        char keyboardKeys_[256];
        char prevKeyboardKeys_[256];
    };
}